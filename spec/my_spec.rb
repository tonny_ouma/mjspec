require "json"
require "date"
require "rspec"
require_relative "spec_helper.rb"
require_relative "..\\pages\\my.rb"
require_relative "..\\db\\RegDB.rb"
include RSpec::Expectations

describe "Shop with MY" do
 
  before(:all) do
	  @goFnames = Array.new
	  @expFnames = Array.new
      getDB = RegDB.new
      
      @expAccounts = getDB.getAccounts('W')
      @goAccounts =  getDB.getAccounts('B')

    while e = @expAccounts.fetch()                 
        actNumber = e[0]
        #getDB.setLoyal(actNumber)        
        ccid = e[2]        
        #getDB.setFraudOverride(actNumber,ccid)
     	@expFnames << e[1]
     end
     
     while g = @goAccounts.fetch()         
        actNumber = g[0]
        #getDB.setLoyal(actNumber)        
        ccid = g[2]        
        #getDB.setFraudOverride(actNumber,ccid)
     	@goFnames << g[1]
     end

       getDB.closeDBConnection      
       
  end 
  	  
 
  before(:each) do
    d = DateTime.now           
    @expExpDate = d.next_month(3).strftime('%m/%d/%Y')
    @expExpDate1yr = d.next_month(15).strftime('%m/%d/%Y')
    @expExpDate5yr = d.next_month(63).strftime('%m/%d/%Y')    
    @expDate = d.next_year(1).strftime('%m/%d/%Y')
    @expDate1yr = d.next_year(2).strftime('%m/%d/%Y')
    @expDate5yr = d.next_year(6).strftime('%m/%d/%Y')
    @shopMy = My.new(@driver)       
  end
  

  it "Buys a GO jack through MY" do
  		     #shop_my_with(jack_type,fname, num_type, renewal_type, ipp_type)                      
	  @shopMy.shop_my_with("SKIP",@goFnames[11], 'SKIP', 'SKIP', 'IPP5')
	  begin
	  	  aggregate_failures "Buys a GO jack through my with @goFnames[5]" do	      
		  expect(@shopMy.ordTot).to eql '47.46'
	  end		  
      rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
       puts e.message
       exit(1)
      end	    
	      
  end
=begin  
  it "Buys a EXPRESS jack through MY" do
  			 #shop_my_with(jack_type,fname, num_type, renewal_type, ipp_type)            
	  @shopMy.shop_my_with("EXPRESS",@expFnames[1], 'US', 'SKIP', 'SKIP')
	  begin
	  	  aggregate_failures "Buys a EXPRESS jack through my" do	      
		  expect(@shopMy.ordTot).to eql '36.68'
	  end		  
      rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
       exit(1)
      end	    
	      
  end

  
   it "Buys a 1yr renewal through MY" do 
    	     #shop_my_with(jack_type,fname, num_type, renewal_type, ipp_type)                     
	  @shopMy.shop_my_with("SKIP", @goFnames[3], 'SKIP', '1yr', 'SKIP')
	  begin
	  	  aggregate_failures "Buys a GO jack through my with @goFnames[5]" do	      
		  expect(@shopMy.ordTot).to eql '37.73'
	  end		  
      rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
       puts e.message
       exit(1)
      end	    
	      
  end
=end  
end
