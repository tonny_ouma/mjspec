require 'oci8'
require_relative 'DBConn.rb'


class RegDB

	def initialize
		@conn = OCI8.new(CONNECTSTRING)
	end

	def getActCodeEmail(fn)
	
	      a = Array.new
   
	      a = @conn.exec("SELECT strarg5 FROM ymax.action WHERE arg = 'ACTIVATIONLETTER' AND act = 1 AND emailaddr = 'STEVE.SCHNEIDER%"+fn+"@MAGICJACK.COM' ORDER BY added")
	   
	     @aCode = a.fetch()	     
	        
	   return  @aCode
	end
	
   def getActCodeSMS(serNum)
	     
	      a = Array.new
   
	      a = @conn.exec("SELECT strarg5 FROM ymax.action WHERE arg = 'ACTIVATIONLETTER' AND act = 1 AND emailaddr = '6152498514' and strarg4 = '"+serNum+"' ORDER BY added")
	   
	      @aCode = a.fetch()
	return @aCode      
	end

	def getCCNum
	
		cardQuery = "SELECT ymaxcreditcardid AS id, cardnumber AS num" +
		            " FROM ymax.ymaxcreditcard " + 
					" WHERE cardnumber NOT IN (SELECT cardnumber FROM ymax.creditcard WHERE cardnumber = ymax.ymaxcreditcard.cardnumber)" +
					" AND used != 'T'" +
					" AND MOD (cardnumber, 2) = 1" +
					" AND SUBSTR (cardnumber, 1, 1) = 4" +
					" AND ROWNUM = 1"		        

		   
	      a = Array.new
   
	      a = @conn.exec(cardQuery)	   
	      @ccNum = a.fetch()
	      
		  @conn.exec("update ymax.ymaxcreditcard set used = 'T' where ymaxcreditcardid = #{@ccNum[0]}")
		  @conn.commit
		  
		return @ccNum[1]
	
	end

	def getMCNum
	
		cardQuery = "SELECT ymaxcreditcardid AS id, cardnumber AS num" +
		            " FROM ymax.ymaxcreditcard " + 
					" WHERE cardnumber NOT IN (SELECT cardnumber FROM ymax.creditcard WHERE cardnumber = ymax.ymaxcreditcard.cardnumber)" +
					" AND used != 'T'" +
					" AND MOD (cardnumber, 2) = 1" +
					" AND SUBSTR (cardnumber, 1, 1) = 5" +
					" AND ROWNUM = 1"		        

		   
	      a = Array.new
   
	      a = @conn.exec(cardQuery)	   
	      @ccNum = a.fetch()
	      
		  @conn.exec("update ymax.ymaxcreditcard set used = 'T' where ymaxcreditcardid = #{@ccNum[0]}")
		  @conn.commit
		  
		return @ccNum[1]
	
	end
	
	def getAccountInfo(fname)
	
	  accountQuery = "select r.accountid, a.locationid, r.cpnid, a.emailaddressid, to_char(r.ENDSERVICE,'mm/dd/yyyy') as endservice, c.cpn_type, a.customer_name, to_char(NVL(i.quantity,0))  " +
					   "from ymax.rsubscriber r, ymax.account a, ymax.cpn c, ymax.ipp i " +
                       "where a.first_name = '" + fname +"'" +
                       " and r.accountid = a.accountid" +
                       " and r.accountid = i.accountid(+)" +                                           
                       " and r.accountid = c.accountid"
                       
       a = Array.new
   
	   a = @conn.exec(accountQuery)
	   
	   @acctInfo = a.fetch()
	   
	   return @acctInfo
	end
	
	def accountid     	                  
         return @acctInfo[0]
    end
    
    def locationid     	                  
         return @acctInfo[1]
    end
    
    def cpnid     	                  
         return @acctInfo[2]
    end
    
    def emailaddressid     	                  
         return @acctInfo[3]
    end
    
    def ordTot     	                  
         return  @ordTot
    end
    
    def expireDate     	                  
         return @acctInfo[4]
    end
    
    def cpnType     	                  
         return @acctInfo[5]
    end
    
    def customerName         	                  
         return @acctInfo[6]
    end
    
    def ippAmt     	                  
         return @acctInfo[7]
    end  
	
	def getMyOrdTot(fname)
	
		ordTotQuery = "select total, added from ymax.orders where accountid = (select accountid from ymax.account where first_name = '"+ fname +"') order by added"		                                  
	    	     
	      a = Array.new
   
	      a = @conn.exec(ordTotQuery)
	   
	      ordTot = a.fetch()
	   
	   tot = "%g" % ("%.2f" % ordTot)
	   
	   return tot
	end
	
	def setLoyal(accountid)
		
		@conn.exec("declare xml varchar2(32676);" + 
          'begin '+
          "xml := '<?xml version=\"1.0\" encoding=\"UTF-8\"?><batch>  <account accountid=\"#{accountid}\" loyal=\"Y\" /></batch>';" +
          "ymax.clientidentifierpackage.set_client_id(\'W2419\'); "+
          "ymax.xmlpackage.xml_process_batch(xml);" +
          "commit;"+
          "end;")
	end
	
    def setFraudOverride(accountid, creditcardid)                  
                 
		 @conn.exec("declare xml varchar2(32676);" + 
           "begin xml := '<\?xml version=\"1.0\" encoding=\"UTF-8\"\?><batch>  <creditcard  accountid=\"#{accountid}\" creditcardid=\"#{creditcardid}\" fraud_override=\"Y\" /></batch>';"+
          " ymax.clientidentifierpackage.set_client_id('W2419'); "+
          " ymax.xmlpackage.xml_process_batch(xml);" +
          " commit;"+
          " end;")
	end
	
	def getAccounts(acctType)
	
	    accounts = Array.new	

        acctQuery = "select a.accountid,  a.first_name, c.creditcardid " +
		   " from ymax.account a, ymax.rsubscriber r, ymax.creditcard c " +
		   " where a.first_name like 'AUT-' || to_char(sysdate, 'yyyy-mm-dd') ||'%' " +
		   " and device_type = '" + acctType + "'" +
		   " and a.accountid = r.accountid" +
		   " and a.accountid = c.accountid" +
		   " order by a.added desc"
		   
		accounts = @conn.exec(acctQuery)		   
		return accounts		
		
	end         
            
		
	def closeDBConnection		
		@conn.logoff
	end

end

