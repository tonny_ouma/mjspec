require 'oci8'
require_relative 'DBConn.rb'

class OrderDetailDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def get_order_detailids(orderid)
		 
		 @orderDetailIds = Array.new		 
		 orderDetailIdQuery = "select orderdetailid from ymax.orderdetail where orderid = #{orderid}"		 
		 @conn.exec(orderDetailIdQuery) do |r| @orderDetailIds.push(r.join(',')) end		  	    
		
	   return @orderDetailIds		 
		 
	end

	def set_order_detail_records(orderid)
		 
		 @orderDetailRecords = Array.new
		 
		 orderDetailRecsQuery = "select orderdetailid," +
								" orderid," +
								" accountid," +
								" productcodeid," + 
								" productcode," +
								" referenceid," +
								" quantity," +
								" to_char(price)," + 
								" to_char(cost)," +
								" to_char(subtotal)," +
								" pieces," +
								" to_char(trunc(added), 'mm/dd/yyyy')," +
								" added_csr," +
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," +
								" to_char(trunc(paydate), 'mm/dd/yyyy')," +
								" detail_type," + 
								" paid," + 
								" invoiceid," +
								" distributionid," +
								" paymentsid," +
								" to_char(collected)," +
								" to_char(collected2)," +
								" orig_distributionid," +
								" found," +
								" rsubscriberid," +
								" added_ip," +
								" updated_ip," +
								" applied," +
								" cpnid," +
								" arg1," +
								" locationid" +
							" from ymax.orderdetail" +
							" where orderid = #{orderid}"				
 				 
		 @conn.exec(orderDetailRecsQuery) do |r| @orderDetailRecords.push(r.join(',')) end		  	    
		
	   return @orderDetailRecords		 
		 
	end

	def set_order_detail_data(orderdetailid)
		
		orderdetailQuery = "select orderdetailid," +
								" orderid," +
								" accountid," +
								" productcodeid," + 
								" productcode," +
								" referenceid," +
								" quantity," +
								" to_char(price)," + 
								" to_char(cost)," +
								" to_char(subtotal)," +
								" pieces," +
								" to_char(trunc(added), 'mm/dd/yyyy')," +
								" added_csr," +
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," +
								" to_char(trunc(paydate), 'mm/dd/yyyy')," +
								" detail_type," + 
								" paid," + 
								" invoiceid," +
								" distributionid," +
								" paymentsid," +
								" to_char(collected)," +
								" to_char(collected2)," +
								" orig_distributionid," +
								" found," +
								" rsubscriberid," +
								" added_ip," +
								" updated_ip," +
								" applied," +
								" cpnid," +
								" arg1," +
								" locationid" +
							" from ymax.orderdetail" +
							" where orderdetailid = #{orderdetailid}"

 		r = @conn.exec(orderdetailQuery)
 		@orderdetailData = r.fetch()		
 			
		return @orderdetailData
	end
	
	 def get_orderdetailid
	 	return @orderdetailData[0]
	 end
	 
	 def get_orderid
	 	return @orderdetailData[1]
	 end
	 
	 def get_accountid
	 	return @orderdetailData[2]
	 end

 	 def get_productcodeid
	 	return @orderdetailData[3]
	 end

	 def get_productcode
	 	return @orderdetailData[4]
	 end
	 
	 def get_referenceid
	 	return @orderdetailData[5]
	 end

	 def get_quantity
	 	return @orderdetailData[6]
	 end
	 
	 def get_price
	 	return @orderdetailData[7]
	 end

	 def get_cost
	 	return @orderdetailData[8]
	 end

	 def get_subtotal
	 	return @orderdetailData[9]
	 end
 
	 def get_pieces
	 	return @orderdetailData[10]
	 end
	 
	 def get_added
	 	return @orderdetailData[11]
	 end
	 
	 def get_added_csr
	 	return @orderdetailData[12]
	 end
	 
	 def get_updated
	 	return @orderdetailData[13]
	 end
	 
	 def get_updated_csr
	 	return @orderdetailData[14]
	 end
	 
	 def get_paydate
	 	return @orderdetailData[15]
	 end

	 def get_detail_type
	 	return @orderdetailData[16]
	 end
	 
	 def get_paid
	 	return @orderdetailData[17]
	 end
	 
	 def get_invoiceid
	 	return @orderdetailData[18]
	 end
	 
	 def get_distributionid
	 	return @orderdetailData[19]
	 end

	 def get_paymentsid
	 	return @orderdetailData[20]
	 end
	 
	 def get_collected
	 	return @orderdetailData[21]
	 end
	 
	 def get_collected2
	 	return @orderdetailData[22]
	 end
	 
	 def get_orig_distributionid
	 	return @orderdetailData[23]
	 end
	 
	 def get_found
	 	return @orderdetailData[24]
	 end
	 
	 def get_rsubscriberid
	 	return @orderdetailData[25]
	 end
	 
	 def get_added_ip
	 	return @orderdetailData[26]
	 end
	 
	 def get_updated_ip
	 	return @orderdetailData[27]
	 end
	 
	 def get_applied
	 	return @orderdetailData[28]
	 end
	 
	 def get_cpnid
	 	return @orderdetailData[29]
	 end

	 def get_arg1
	 	return @orderdetailData[30]
	 end
	 
	 def get_locationid
	 	return @orderdetailData[31]
	 end
	 
    def closeDBConnection		
		@conn.logoff
	end
end
