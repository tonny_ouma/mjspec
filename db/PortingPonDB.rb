require 'oci8'
require_relative 'DBConn.rb'

class PortingPonDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
#		@conn = OCI8.new('winman/winnat0309@//develop.talk4free.com:1521/ddb01')	
	end	

	def set_portingpon_data(id)

		     portingponQuery = "select to_char(id)," +
								" pon," +
								" ccna," +
								" to_char(cpnid)," +
								" to_char(pon_type)," +
								" to_char(porting_id)," +
								" added_csr," +
								" added_ip," +
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," + 
								" updated_ip," +
								" to_char(trunc(added), 'mm/dd/yyyy')," +                  
								" to_char(pon_status)," +
								" carrier_account_number," +
								" carrier_passcode," +
								" to_char(locationid)," +
								" carrier_spid," +
								" to_char(rsubscriberid)," +                   
								" to_char(trunc(due_date), 'mm/dd/yyyy')," +
								" claimed_carrier," +
								" to_char(accountid)," +
								" to_char(orderid)," +
								" to_char(last_rejectedid)," + 
								" to_char(sup_count)," +
								" electronic_order," +
								" orderdetailid," +
								" billing_cancel_response," +
								" prevalcount," +
								" mainnumber" +
						        " from  ymax.porting_pon" +
						        " where ID = #{id}"
						
		r = @conn.exec(portingponQuery)

		@portingponData = r.fetch()
		
		return @portingponData
	 end

  	def get_id
	   return @portingponData[0]
	end
	
	def get_pon
	   return @portingponData[1]
	end
	
	def get_ccna
		return @portingponData[2]
	end
	
	def get_cpnid
	   return @portingponData[3]
	end

	def get_pon_type
	   return @portingponData[4]
	end
	
	def get_porting_id
	   return @portingponData[5]
	end

	def get_added_csr
	   return @portingponData[6]
	end
	
	def get_added_ip
	   return @portingponData[7]
	end

	def get_updated
	   return @portingponData[8]
	end
	
	def get_updated_csr
	   return @portingponData[9]
	end

	def get_updated_ip
	   return @portingponData[10]
	end
	
	def get_added
	   return @portingponData[11]
	end
	
	def get_pon_status
	   return @portingponData[12]
	end

	def get_carrier_account_number
	   return @portingponData[13]
	end
	
	def get_carrier_passcode
	   return @portingponData[14]
	end
	
	def get_locationid
	   return @portingponData[15]
	end
	
	def get_carrier_spid
	   return @portingponData[16]
	end
	
	def get_rsubscriberid
	   return @portingponData[17]
	end
	
	def get_due_date
	   return @portingponData[18]
	end
	
	def get_claimed_carrier
	   return @portingponData[19]
	end
	
	def get_accountid
	   return @portingponData[20]
	end
	
	def get_orderid
	   return @portingponData[21]
	end
	
	def get_last_rejectedid
	   return @portingponData[22]
	end

	def get_sup_count
	   return @portingponData[23]
	end
	
	def get_electronic_order
	   return @portingponData[24]
	end
	
	def get_orderdetailid
	   return @portingponData[25]
	end
	
	def get_billing_cancel_response
	   return @portingponData[26]
	end
	
	def get_prevalcount
	   return @portingponData[27]
	end
	
	def get_mainnumber
	   return @portingponData[28]
	end
		
    def closeDBConnection		
		@conn.logoff
	end
end
