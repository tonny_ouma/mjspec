require 'oci8'
require_relative 'DBConn.rb'

class ReclaimJackDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_reclaim_jack_data(reclaimjackid)
						  
		reclaimjackQuery ="select reclaimjackid, "+
									"serial_no, "+
									"accountid, "+
									"rsubscriberid, "+
									"cpnid, "+
									"enumber, "+
									"calling_party_no, "+
									"error_type, "+
									"suberror_type, "+
									"to_char(added,'mm/dd/yyyy'), "+
									"added_csr, "+
									"to_char(updated,'mm/dd/yyyy'), "+
									"updated_csr, "+
									"added_ip, "+
									"updated_ip, "+
									"return_source "+     
							"from ymax.reclaimjack "   +
							"where reclaimjackid = #{reclaimjackid}"
							 
		r = @conn.exec(reclaimjackQuery)
		
		@reclaimjackData = r.fetch()
		
		return @reclaimjackData
	end
				
	def get_reclaimjackid
	  return @reclaimjackData[0]
	end
				
	def get_serial_no
	  return @reclaimjackData[1]
	end
				
	def get_accountid
	  return @reclaimjackData[2]
	end
				
	def get_rsubscriberid
	  return @reclaimjackData[3]
	end
				
	def get_cpnid
	  return @reclaimjackData[4]
	end
				
	def get_enumber
	  return @reclaimjackData[5]
	end
				
	def get_calling_party_no
	  return @reclaimjackData[6]
	end
				
	def get_error_type
	  return @reclaimjackData[7]
	end
				
	def get_suberror_type
	 return @reclaimjackData[8]
	end
				
	def get_added
	  return @reclaimjackData[9]
	end
				
	def get_added_csr
	  return @reclaimjackData[10]
	end
				
	def get_updated
	  return @reclaimjackData[11]
	end
				
	def get_updated_csr
	  return @reclaimjackData[12]
	end
				
	def get_added_ip
	  return @reclaimjackData[13]
	end
				
	def get_updated_ip
	  return @reclaimjackData[14]
	end
				
	def get_return_source
	  return @reclaimjackData[15]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end