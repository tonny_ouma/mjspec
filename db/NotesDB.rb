require 'oci8'
require_relative 'DBConn.rb'

class NotesDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_notes_records(accountid)
		 
		@notesRecords = Array.new
		 
		notesRecsQuery =  "select notesid," +
			  		" accountid," +
			  		" cpnid," +          
			  		" rsubscriberid," +  
			  		" note_type," +      
			  		" dbms_lob.substr(notetext,3000,1)" +
			  		" html," +           
			  		" webuserid," +      
			  		" csr," +            
			  		" to_char(added,'mm/dd/yyyy')," +          
			  		" added_csr," +      
			  		" to_char(updated,'mm/dd/yyyy')," +        
			  		" updated_csr," +    
			  		" to_char(note_date,'mm/dd/yyyy')" +      
			  		" active," +         
			  		" added_ip," +       
			  		" updated_ip," +     
			  		" ticket" +         
				" from ymax.notes" +
				" where accountid = #{accountid}"
 				 
		 @conn.exec(notesRecsQuery) do |r| @notesRecords.push(r.join(',')) end		  	    
		
	   return @notesRecords		 
		 
	end	


	def set_notes_data(notesid)
		notesQuery ="select notesid," +
			  		" accountid," +
			  		" cpnid," +          
			  		" rsubscriberid," +  
			  		" note_type," +      
			  		" dbms_lob.substr(notetext,3000,1)" +
			  		" html," +           
			  		" webuserid," +      
			  		" csr," +            
			  		" to_char(added,'mm/dd/yyyy')," +          
			  		" added_csr," +      
			  		" to_char(updated,'mm/dd/yyyy')," +        
			  		" updated_csr," +    
			  		" to_char(note_date,'mm/dd/yyyy')" +      
			  		" active," +         
			  		" added_ip," +       
			  		" updated_ip," +     
			  		" ticket" +         
				" from ymax.notes" +
				" where notesid = #{notesid}"
		n = @conn.exec(notesQuery)
		
		@notesData = n.fetch()
		
		return @notesData
	end
	
	def get_notesid
		return @notesData[0]
	end

	def get_accountid
		return @notesData[1]
	end

	def get_cpnid          
		return @notesData[2]
	end

	def get_rsubscriberid  
		return @notesData[3]
	end

	def get_note_type      
		return @notesData[4]
	end

	def get_notetext
		return @notesData[5]
	end

	def get_html           
		return @notesData[6]
	end

	def get_webuserid      
		return @notesData[7]
	end

	def get_csr          
		return @notesData[8]
	end

	def get_added          
		return @notesData[9]
	end

	def get_added_csr      
		return @notesData[10]
	end

	def get_updated        
		return @notesData[11]
	end

	def get_updated_csr    
		return @notesData[12]
	end

	def get_note_date      
		return @notesData[13]
	end

	def get_active         
		return @notesData[14]
	end

	def get_added_ip       
		return @notesData[15]
	end

	def get_updated_ip     
		return @notesData[16]
	end

	def get_ticket
		return @notesData[17]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end