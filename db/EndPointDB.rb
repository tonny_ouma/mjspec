require 'oci8'
require_relative 'DBConn.rb'

class EndPointDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_endpoint_data(endpointid)
						  
		endpointQuery ="select endpointid," +
					   " accountid," +                                              
					   " subscriberid," +               
					   " cpnid," +                      
					   " enumber," + 
					   " rgid," +                       
					   " admin_state," +                
					   " to_char(added,'mm/dd/yyyy')," +                      
					   " password," +                   
					   " added_csr," +                  
					   " to_char(updated,'mm/dd/yyyy')," +                      
					   " updated_csr," +                
					   " added_ip," +                   
					   " updated_ip," +                 
					   " subtype," +                    
					   " sip_proxy_host," +             
					   " sip_proxy_port," +             
					   " spoof," +                      
					   " locationid," +                 
					   " serial_no," +                  
					   " position," +                   
					   " to_char(subscriptionid)," +             
					   " device_type," +                
					   " serviceplan_productcodeid," +  
					   " routing," +                    
					   " to_char(last_password_reset,'mm/dd/yyyy')," +        
					   " to_char(treatments)," +                 
					   " vmtimer," +                    
					   " calling_party_no," +           
					   " usagebits," +                  
					   " cpn_type," +                   
					   " intrado_arg," +                
					   " e911_color," +                 
					   " e911_description," +           
					   " to_char(e911_verified,'mm/dd/yyyy')," +              
					   " to_char(e911_status)" +                
					" from ymax.endpoint" +
					" where endpointid = #{endpointid}"
					
		r = @conn.exec(endpointQuery)
		
		@endpointData = r.fetch()
		
		return @endpointData
	end
	
	def get_endpointid
		return @endpointData[0]
	end

	def get_accountid
		return @endpointData[1]		 
	end
	 
	def get_subscriberid
		return @endpointData[2]		 
	end
	 
	def get_cpnid
		return @endpointData[3]		 
	end
	 
	def get_enumber
		return @endpointData[4]		 
	end
	 
	def get_rgid
		return @endpointData[5]		 
	end
	 
	def get_admin_state
		return @endpointData[6]		 
	end
	 
	def get_added
		return @endpointData[7]		 
	end
	 
	def get_password
		return @endpointData[8]		 
	end
	 
	def get_added_csr
		return @endpointData[9]		 
	end
	 
	def get_updated
		return @endpointData[10]		 
	end
	 
	def get_updated_csr
		return @endpointData[11]		 
	end
	 
	def get_added_ip
		return @endpointData[12]		 
	end
	 
	def get_updated_ip
		return @endpointData[13]		 
	end
	 
	def get_subtype
		return @endpointData[14]		 
	end
	 
	def get_sip_proxy_host
		return @endpointData[15]		 
	end
	 
	def get_sip_proxy_port
		return @endpointData[16]		 
	end
	 
	def get_spoof
		return @endpointData[17]		 
	end
	 
	def get_locationid
		return @endpointData[18]		 
	end
	 
	def get_serial_no
		return @endpointData[19]		 
	end
	 
	def get_position
		return @endpointData[20]		 
	end
	 
	def get_subscriptionid
		return @endpointData[21]		 
	end
	 
	def get_device_type
		return @endpointData[22]		 
	end
	 
	def get_serviceplan_productcodeid
		return @endpointData[23]		 
	end
	 
	def get_routing
		return @endpointData[24]		 
	end
	 
	def get_last_password_reset
		return @endpointData[25]		 
	end
	 
	def get_treatments
		return @endpointData[26]		 
	end
	 
	def get_vmtimer
		return @endpointData[27]		 
	end
	 
	def get_calling_party_no
		return @endpointData[28]		 
	end
	 
	def get_usagebits
		return @endpointData[29]		 
	end
	 
	def get_cpn_type
		return @endpointData[30]		 
	end

	def get_intrado_arg
		return @endpointData[31]		 
	end

	def get_e911_color
		return @endpointData[32]		 
	end
	 
	def get_e911_description
		return @endpointData[33]		 
	end
	 
	def get_e911_verified
		return @endpointData[34]		 
	end
	 
	def get_e911_status
		return @endpointData[35]		 
	end
    
    def closeDBConnection		
		@conn.logoff
	end
	
end