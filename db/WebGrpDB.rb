require 'oci8'
require_relative 'DBConn.rb'

class WebGrpDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_webgrp_data(groupid)
		webgrpQuery =	"select groupid," +			  	
			   		  	" name," +    
			   		  	" description," +       
			  		  	" to_char(added,'mm/dd/yyyy')," + 
			  		  	" added_csr," +      
			  		  	" added_ip," +          
			  		  	" to_char(updated,'mm/dd/yyyy')," +           
			  		  	" updated_csr," + 
			  		  	" updated_ip" +  
					  	" from ymax.webgroup" +
					  	" where groupid = #{groupid}"

		g = @conn.exec(webgrpQuery)
		
		@webgrpData = g.fetch()
		
		return @webgrpData
	end
	

	def get_groupid           
		return @webgrpData[0]
	end

	def get_name           
		return @webgrpData[1]
	end

	def get_description           
		return @webgrpData[2]
	end

	def get_added 
		return @webgrpData[3]
	end

	def get_added_csr      
		return @webgrpData[4]
	end

	def get_added_ip          
		return @webgrpData[5]
	end

	def get_updated           
		return @webgrpData[6]
	end

	def get_updated_csr 
		return @webgrpData[7]
	end

	def get_updated_ip  	
		return @webgrpData[8]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end