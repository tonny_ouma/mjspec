require 'oci8'
require_relative 'DBConn.rb'

class ProductCodeDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_product_code_data(productCodeId)

    productcodeQuery = "select shortdesc, "+
								"upccode, "+
								"subcategory, "+
								"comments, "+
								"initial_term, "+
								"multiplier, "+
								"cust_adjustable, "+
								"class4, "+
								"gift_productcodeid, "+
								"paynow_productcodeid, "+
								"suppress, "+
								"shippingcategory, "+
								"deliverycategory, "+
								"weight, "+
								"max_companion_quantity, "+
								"companion_productcodeid, "+
								"exceed_device, "+
								"max_order_quantity, "+
								"e911, "+
								"class3, "+
								"class2, "+
								"tangible, "+
								"authgroup, "+
								"class1, "+
								"to_char(referral_levels), "+
								"on_hold, "+
								"installment_amount, "+
								"no_installments, "+
								"downpayment, "+
								"renewal_productcodeid, "+
								"feeproductcodeid, "+
								"serviceplan_type, "+
								"renewal_price, "+
								"vanity, "+
								"lettername, "+
								"sendconfirmletter, "+
								"freerushshipping, "+
								"updated_ip, "+
								"to_char(updated,'mm/dd/yyyy'), "+
								"added_ip, "+
								"to_char(added,'mm/dd/yyyy'), "+
								"active, "+
								"taxproductcodeid, "+
								"zipcomm_item, "+
								"zipcomm_grp, "+
								"adjustable, "+
								"updated_csr, "+
								"added_csr, "+
								"sortorder, "+
								"product, "+
								"trialdays, "+
								"glaccount, "+
								"to_char(disc_enddate,'mm/dd/yyyy'), "+
								"to_char(disc_startdate,'mm/dd/yyyy'), "+
								"disc_type, "+
								"disc, "+
								"pieces, "+
								"cost, "+
								"price, "+
								"description, "+
								"name, "+
								"category, "+
								"productcode, "+
								"productcodeid, "+
								"display_price, "+
								"product_type, "+
								"max_lines, "+
								"required_productcodeid, "+
								"recurring_months, "+
								"feature, "+
								"promovalue, "+
								"image, "+
								"apple_item_id, "+
								"rgid, "+
								"google_productcode, "+
								"apple_productcode, "+
								"allow_ymaxcredit "+
			              "from ymax.productcode where productcodeid = #{productCodeId}"   
        
        r = @conn.exec(productcodeQuery)
		
		@productcodeData = r.fetch()
		
		return @productcodeData
                    
	end
                    
	def get_shortdesc
		return @productcodeData[0]
	end
			
	def get_upccode
		return @productcodeData[1]
	end
			
	def get_subcategory
		return @productcodeData[2]
	end
			
	def get_comments
		return @productcodeData[3]
	end
			
	def get_initial_term
		return @productcodeData[4]
	end
			
	def get_multiplier
		return @productcodeData[5]
	end
			
	def get_cust_adjustable
		return @productcodeData[6]
	end
			
	def get_class4
		return @productcodeData[7]
	end
			
	def get_gift_productcodeid
		return @productcodeData[8]
	end
			
	def get_paynow_productcodeid
		return @productcodeData[9]
	end
			
	def get_suppress
		return @productcodeData[10]
	end
			
	def get_shippingcategory
		return @productcodeData[11]
	end
			
	def get_deliverycategory
		return @productcodeData[12]
	end
			
	def get_weight
		return @productcodeData[13]
	end
			
	def get_max_companion_quantity
		return @productcodeData[14]
	end
			
	def get_companion_productcodeid
		return @productcodeData[15]
	end
			
	def get_exceed_device
		return @productcodeData[16]
	end
			
	def get_max_order_quantity
		return @productcodeData[17]
	end
			
	def get_e911
		return @productcodeData[18]
	end
			
	def get_class3
		return @productcodeData[19]
	end
			
	def get_class2
		return @productcodeData[20]
	end
			
	def get_tangible
		return @productcodeData[21]
	end
			
	def get_authgroup
		return @productcodeData[22]
	end
			
	def get_class1
		return @productcodeData[23]
	end
			
	def get_referral_levels
		return @productcodeData[24]
	end
			
	def get_on_hold
		return @productcodeData[25]
	end
			
	def get_installment_amount
		return @productcodeData[26]
	end
			
	def get_no_installments
		return @productcodeData[27]
	end
			
	def get_downpayment
		return @productcodeData[28]
	end
			
	def get_renewal_productcodeid
		return @productcodeData[29]
	end
			
	def get_feeproductcodeid
		return @productcodeData[30]
	end
			
	def get_serviceplan_type
		return @productcodeData[31]
	end
			
	def get_renewal_price
		return @productcodeData[32]
	end
			
	def get_vanity
		return @productcodeData[33]
	end
			
	def get_lettername
		return @productcodeData[34]
	end
			
	def get_sendconfirmletter
		return @productcodeData[35]
	end
			
	def get_freerushshipping
		return @productcodeData[36]
	end
			
	def get_updated_ip
		return @productcodeData[37]
	end
			
	def get_updated
		return @productcodeData[38]
	end
			
	def get_added_ip
		return @productcodeData[39]
	end
			
	def get_added
		return @productcodeData[40]
	end
			
	def get_active
		return @productcodeData[41]
	end
			
	def get_taxproductcodeid
		return @productcodeData[42]
	end
			
	def get_zipcomm_item
		return @productcodeData[43]
	end
			
	def get_zipcomm_grp
		return @productcodeData[44]
	end
			
	def get_adjustable
		return @productcodeData[45]
	end
			
	def get_updated_csr
		return @productcodeData[46]
	end
			
	def get_added_csr
		return @productcodeData[47]
	end
			
	def get_sortorder
		return @productcodeData[48]
	end
			
	def get_product
		return @productcodeData[49]
	end
			
	def get_trialdays
		return @productcodeData[50]
	end
			
	def get_glaccount
		return @productcodeData[51]
	end
			
	def get_disc_enddate
		return @productcodeData[52]
	end
			
	def get_disc_startdate
		return @productcodeData[53]
	end
			
	def get_disc_type
		return @productcodeData[54]
	end
			
	def get_disc
		return @productcodeData[55]
	end
			
	def get_pieces
		return @productcodeData[56]
	end
			
	def get_cost
		return @productcodeData[57]
	end
			
	def get_price
		return @productcodeData[58]
	end
			
	def get_description
		return @productcodeData[59]
	end
			
	def get_name
		return @productcodeData[60]
	end
			
	def get_category
		return @productcodeData[61]
	end
			
	def get_productcode
		return @productcodeData[62]
	end
			
	def get_productcodeid
		return @productcodeData[63]
	end
			
	def get_display_price
		return @productcodeData[64]
	end
			
	def get_product_type
		return @productcodeData[65]
	end
			
	def get_max_lines
		return @productcodeData[66]
	end
			
	def get_required_productcodeid
		return @productcodeData[67]
	end
			
	def get_recurring_months
		return @productcodeData[68]
	end
			
	def get_feature
		return @productcodeData[69]
	end
			
	def get_promovalue
		return @productcodeData[70]
	end
			
	def get_image
		return @productcodeData[71]
	end
			
	def get_apple_item_id
		return @productcodeData[72]
	end
			
	def get_rgid
		return @productcodeData[73]
	end
			
	def get_google_productcode
		return @productcodeData[74]
	end
			
	def get_apple_productcode
		return @productcodeData[75]
	end
			
	def get_allow_ymaxcredit
		return @productcodeData[76]
	end
			
	def closeDBConnection		
		@conn.logoff
	end
end
    