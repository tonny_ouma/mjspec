require 'oci8'
require_relative 'DBConn.rb'

class CurrentDeviceInfoDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
	end	

	def set_current_device_info_data(currentdeviceinfoid)

		currentdeviceinfoQuery = "select currentdeviceinfoid," +
								" serial_no," +
								" usage_type," +
								" device_type," +
								" device_hw," +
								" device_mfg," +
								" device_brand," +
								" device_osversion," +
								" osname," +
								" minimum_software_version," +
								" current_software_version," +
								" module_version," +                   
								" to_char(trunc(last_provisioning), 'mm/dd/yyyy')," +
								" last_provisioning_ip," +
								" last_accountid," +
								" to_char(trunc(last_upgrade), 'mm/dd/yyyy')," + 
								" to_char(trunc(prev_provisioning), 'mm/dd/yyyy')," +
								" prev_provisioning_ip," +
								" prev_accountid," +
								" to_char(trunc(prev_upgrade), 'mm/dd/yyyy')," +
								" to_char(trunc(added), 'mm/dd/yyyy')," +
								" added_csr," +
								" added_ip," + 
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," + 
								" updated_ip" +
							" from  ymax.currentdeviceinfo" +
							" where currentdeviceinfoid = #{currentdeviceinfoid}"

		c = @conn.exec(currentdeviceinfoQuery)

		@currentdeviceinfoData = c.fetch()
		
		return @currentdeviceinfoData
	 end

	def get_currentdeviceinfoid
		return @currentdeviceinfoData[0]
	end
	
	def get_serial_no
	   return @currentdeviceinfoData[1]
	end
	
	def get_usage_type
	   return @currentdeviceinfoData[2]
	end
	
	def get_device_type
	   return @currentdeviceinfoData[3]
	end

	def get_device_hw
	   return @currentdeviceinfoData[4]
	end
	
	def get_device_mfg
	   return @currentdeviceinfoData[5]
	end

	def get_device_brand
	   return @currentdeviceinfoData[6]
	end
	
	def get_device_osversion
	   return @currentdeviceinfoData[7]
	end

	def get_osname
	   return @currentdeviceinfoData[8]
	end
	
	def get_minimum_software_version
	   return @currentdeviceinfoData[9]
	end

	def get_current_software_version
	   return @currentdeviceinfoData[10]
	end
	
	def get_module_version
	   return @currentdeviceinfoData[11]
	end
	
	def get_last_provisioning
	   return @currentdeviceinfoData[12]
	end

	def get_last_provisioning_ip
	   return @currentdeviceinfoData[13]
	end
	
	def get_last_accountid
	   return @currentdeviceinfoData[14]
	end
	
	def get_last_upgrade
	   return @currentdeviceinfoData[15]
	end
	
	def get_prev_provisioning
	   return @currentdeviceinfoData[16]
	end
	
	def get_prev_provisioning_ip
	   return @currentdeviceinfoData[17]
	end
	
	def get_prev_accountid
	   return @currentdeviceinfoData[18]
	end
	
	def get_prev_upgrade
	   return @currentdeviceinfoData[19]
	end
	
	def get_added
	   return @currentdeviceinfoData[20]
	end
	
	def get_added_csr
	   return @currentdeviceinfoData[21]
	end
	
	def get_added_ip
	   return @currentdeviceinfoData[22]
	end

	def get_updated
	   return @currentdeviceinfoData[23]
	end
	
	def get_updated_csr
	   return @currentdeviceinfoData[24]
	end
	
	def get_updated_ip
	   return @currentdeviceinfoData[25]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
