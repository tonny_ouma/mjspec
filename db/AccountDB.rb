require 'oci8'
require_relative 'DBConn.rb'

class AccountDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_account_data(fname)
	
		accountQuery =	"select accountid,"+          
						" locationid,"+         
						" emailaddressid,"+     
						" account_type, "+
						" busres,"+   
						" customer_name,"+
						" to_char(balance),"+  
						" to_char(payment_due,'mm/dd/yyyy'),"+
						" to_char(invoices),"+
						" to_char(last_invoice,'mm/dd/yyyy'),"+
						" to_char(payments),"+
						" to_char(last_payment,'mm/dd/yyyy'),"+
						" to_char(refunds),"+
						" to_char(last_refund,'mm/dd/yyyy'),"+
						" to_char(debits),"+
						" to_char(last_debit),"+
						" to_char(credits),"+
						" to_char(last_credit,'mm/dd/yyyy'),"+
						" cpni_flg,"+
						" active,"+
						" to_char(order_date,'mm/dd/yyyy'),"+
						" order_csr,"+
						" order_ipaddr,"+
						" order_tracking_no,"+
						" csr,"+
						" to_char(added,'mm/dd/yyyy'),"+
						" added_csr,"+
						" to_char(updated,'mm/dd/yyyy'),"+
						" updated_csr,"+
						" fraud_account,"+
						" added_ip,"+
						" updated_ip,"+
						" sip_iid,"+
						" ask_replace,"+
						" hu,"+
						" to_char(referrer_accountid),"+
						" inbound_referrerdetailid,"+
						" loyal,"+
						" chargebacks,"+
						" nflags,"+
						" nocontact,"+
						" to_char(treatments),"+
						" vip,"+
						" first_name,"+
						" last_name,"+
						" website"+  
					" from ymax.account" +
					" where first_name ='"+fname+"'"
	
		a = @conn.exec(accountQuery)
		
		@accountData = a.fetch()
		
		return @accountData
	 end	 
	 
	 def get_accountid
	 	return @accountData[0]
	 end
	 
	 def get_locationid
	 	return @accountData[1]
	 end
	 
	 def get_emailaddressid
	 	return @accountData[2]
	 end

	 def get_account_type
	 	return @accountData[3]
	 end

	 def get_busres
	 	return @accountData[4]
	 end
	 
	 def get_customer_name
	 	return @accountData[5]
	 end
	 
	 def get_balance
	 	return @accountData[6]
	 end

	 def get_payment_due
	 	return @accountData[7]
	 end
	 
	 def get_invoices
	 	return @accountData[8]
	 end

	 def get_last_invoice
	 	return @accountData[9]
	 end
	 
	 def get_payments
	 	return @accountData[10]
	 end
	 
	 def get_last_payment
	 	return @accountData[11]
	 end
	 
	 def get_refunds
	 	return @accountData[12]
	 end
	 
	 def get_last_refund
	 	return @accountData[13]
	 end
	 
	 def get_debits
	 	return @accountData[14]
	 end
	 
	 def get_last_debit
	 	return @accountData[15]
	 end
	 
	 def get_credits
	 	return @accountData[16]
	 end
	 
	 def get_last_credit
	 	return @accountData[17]
	 end
	 
	 def get_cpni_flg
	 	return @accountData[18]
	 end
	 
	 def get_active
	 	return @accountData[19]
	 end
	 
	 def get_order_date
	 	return @accountData[20]
	 end
	 
	 def get_order_csr
	 	return @accountData[21]
	 end
	 
	 def get_order_ipaddr
	 	return @accountData[22]
	 end
	 
	 def get_order_tracking_no
	 	return @accountData[23]
	 end
	 
	 def get_csr
	 	return @accountData[24]
	 end
	 
	 def get_added
	 	return @accountData[25]
	 end
	 
	 def get_added_csr
	 	return @accountData[26]
	 end

	 def get_updated
	 	return @accountData[27]
	 end
	 
	 def get_updated_csr
	 	return @accountData[28]
	 end
	 
	 def get_fraud_account
	 	return @accountData[29]
	 end
	 
	 def get_added_ip
	 	return @accountData[30]
	 end
	 
	 def get_updated_ip
	 	return @accountData[31]
	 end
	 
	 def get_sip_iid
	 	return @accountData[32]
	 end
	 
	 def get_ask_replace
	 	return @accountData[33]
	 end
	 
	 def get_hu
	 	return @accountData[34]
	 end
	 
	 def get_referrer_accountid
	 	return @accountData[35]
	 end
	 
	 def get_inbound_referrerdetailid
	 	return @accountData[36]
	 end
	 
	 def get_loyal
	 	return @accountData[37]
	 end
	 
	 def get_chargebacks
	 	return @accountData[38]
	 end
	 
	 def get_nflags
	 	return @accountData[39]
	 end
	 
	 def get_nocontact
	 	return @accountData[40]
	 end
	 
	 def get_treatments
	 	return @accountData[41]
	 end
	 
	 def get_vip
	 	return @accountData[42]
	 end
	 
	 def get_first_name
	 	return @accountData[43]
	 end
	 
	 def get_last_name
	 	return @accountData[44]
	 end
	 
	 def get_website
	 	return @accountData[45]
	 end	  
	 
    def closeDBConnection		
		@conn.logoff
	end
	
end