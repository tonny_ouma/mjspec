require 'oci8'
require_relative 'DBConn.rb'

class OrderTaxDetailDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
	end	

	def set_order_tax_detail_data(ordertaxdetailid)

		ordertaxdetailQuery = "select ordertaxdetailid," +
									" orderid," +
									" accountid," +
									" orderdetailid," +
									" referenceid," +
									" geocode," +
									" inoutcity," +
									" inoutlocal," +
									" effdate," +
									" to_char(taxrate)," +
									" taxauth," +                   
									" taxtype," +
									" taxcat," +
									" description," +
									" to_char(fee)," +
									" to_char(taxableamt)," +
									" to_char(quantity)," + 
									" to_char(subtotal)," +
									" detail_type," +
									" to_char(trunc(added), 'mm/dd/yyyy')," +
									" added_csr," +
									" added_ip," + 
									" to_char(trunc(updated), 'mm/dd/yyyy')," +
									" updated_csr," + 
									" updated_ip," +
									" to_char(trunc(paid_through), 'mm/dd/yyyy')," +
									" to_char(trunc(paydate), 'mm/dd/yyyy')" +
								" from  ymax.ordertaxdetail" +
								" where ordertaxdetailid = #{ordertaxdetailid}"

		r = @conn.exec(ordertaxdetailQuery)

		@ordertaxdetailData = r.fetch()
		
		return @ordertaxdetailData
	 end

 	def get_ordertaxdetailid
	   return @ordertaxdetailData[0]
	end
	
	def get_orderid
	   return @ordertaxdetailData[1]
	end
	
	def get_accountid
	   return @ordertaxdetailData[2]
	end
	
	def get_orderdetailid
	   return @ordertaxdetailData[3]
	end

	def get_referenceid
	   return @ordertaxdetailData[4]
	end
	
	def get_geocode
	   return @ordertaxdetailData[5]
	end

	def get_inoutcity
	   return @ordertaxdetailData[6]
	end
	
	def get_inoutlocal
	   return @ordertaxdetailData[7]
	end

	def get_effdate
	   return @ordertaxdetailData[8]
	end
	
	def get_taxrate
	   return @ordertaxdetailData[9]
	end

	def get_taxauth
	   return @ordertaxdetailData[10]
	end
	
	def get_taxtype
	   return @ordertaxdetailData[11]
	end
	
	def get_taxcat
	   return @ordertaxdetailData[12]
	end

	def get_description
	   return @ordertaxdetailData[13]
	end
	
	def get_fee
	   return @ordertaxdetailData[14]
	end
	
	def get_unittype
	   return @ordertaxdetailData[15]
	end
	
	def get_taxableamt
	   return @ordertaxdetailData[16]
	end
	
	def get_quantity
	   return @ordertaxdetailData[17]
	end
	
	def get_subtotal
	   return @ordertaxdetailData[18]
	end
	
	def get_detail_type
	   return @ordertaxdetailData[19]
	end
	
	def get_added
	   return @ordertaxdetailData[20]
	end
	
	def get_added_csr
	   return @ordertaxdetailData[21]
	end
	
	def get_added_ip
	   return @ordertaxdetailData[22]
	end

	def get_updated
	   return @ordertaxdetailData[23]
	end
	
	def get_updated_csr
	   return @ordertaxdetailData[24]
	end
	
	def get_updated_ip
	   return @ordertaxdetailData[25]
	end
	
	def get_paid_through
	   return @ordertaxdetailData[26]
	end
	
	def get_paydate
	   return @ordertaxdetailData[27]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
