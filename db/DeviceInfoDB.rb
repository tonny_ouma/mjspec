require 'oci8'
require_relative 'DBConn.rb'

class DeviceInfoDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
	end	

	def set_device_info_data(deviceinfoid)

		deviceinfoQuery = "select deviceinfoid," +
								" serial_no," +
								" usage_type," +
								" device_type," +
								" device_hw," +
								" device_mfg," +
								" device_brand," +
								" device_osversion," +
								" osname," +
								" minimum_software_version," +
								" current_software_version," +
								" module_version," +                   
								" to_char(trunc(last_provisioning), 'mm/dd/yyyy')," +
								" last_provisioning_ip," +
								" last_accountid," +
								" to_char(trunc(last_upgrade), 'mm/dd/yyyy')," + 
								" to_char(trunc(prev_provisioning), 'mm/dd/yyyy')," +
								" prev_provisioning_ip," +
								" prev_accountid," +
								" to_char(trunc(prev_upgrade), 'mm/dd/yyyy')," +
								" to_char(trunc(added), 'mm/dd/yyyy')," +
								" added_csr," +
								" added_ip," + 
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," + 
								" updated_ip," +
								" current_exists," +                  
								" softphone_uuid," + 
								" active," +
								" prov_iid," +
								" android_release," + 
								" android_sdkint," +
								" android_fprint" +
							" from  ymax.deviceinfo" +
							" where deviceinfoid = #{deviceinfoid}"

		r = @conn.exec(deviceinfoQuery)

		@deviceinfoData = r.fetch()
		
		return @deviceinfoData
	 end

	def get_deviceinfoid
		return @deviceinfoData[0]
	end
	
	def get_serial_no
	   return @deviceinfoData[1]
	end
	
	def get_usage_type
	   return @deviceinfoData[2]
	end
	
	def get_device_type
	   return @deviceinfoData[3]
	end

	def get_device_hw
	   return @deviceinfoData[4]
	end
	
	def get_device_mfg
	   return @deviceinfoData[5]
	end

	def get_device_brand
	   return @deviceinfoData[6]
	end
	
	def get_device_osversion
	   return @deviceinfoData[7]
	end

	def get_osname
	   return @deviceinfoData[8]
	end
	
	def get_minimum_software_version
	   return @deviceinfoData[9]
	end

	def get_current_software_version
	   return @deviceinfoData[10]
	end
	
	def get_module_version
	   return @deviceinfoData[11]
	end
	
	def get_last_provisioning
	   return @deviceinfoData[12]
	end

	def get_last_provisioning_ip
	   return @deviceinfoData[13]
	end
	
	def get_last_accountid
	   return @deviceinfoData[14]
	end
	
	def get_last_upgrade
	   return @deviceinfoData[15]
	end
	
	def get_prev_provisioning
	   return @deviceinfoData[16]
	end
	
	def get_prev_provisioning_ip
	   return @deviceinfoData[17]
	end
	
	def get_prev_accountid
	   return @deviceinfoData[18]
	end
	
	def get_prev_upgrade
	   return @deviceinfoData[19]
	end
	
	def get_added
	   return @deviceinfoData[20]
	end
	
	def get_added_csr
	   return @deviceinfoData[21]
	end
	
	def get_added_ip
	   return @deviceinfoData[22]
	end

	def get_updated
	   return @deviceinfoData[23]
	end
	
	def get_updated_csr
	   return @deviceinfoData[24]
	end
	
	def get_updated_ip
	   return @deviceinfoData[25]
	end

	def get_current_exists
	   return @deviceinfoData[26]
	end
	
	def get_softphone_uuid
	   return @deviceinfoData[27]
	end
	
	def get_subtotal
	   return @deviceinfoData[28]
	end
	
	def get_active
	   return @deviceinfoData[29]
	end
	
	def get_prov_iid
	   return @deviceinfoData[30]
	end
	
	def get_android_release
	   return @deviceinfoData[31]
	end
	
	def get_android_sdkint
	   return @deviceinfoData[32]
	end

	def get_android_fprint
	   return @deviceinfoData[33]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
