require 'oci8'
require_relative 'DBConn.rb'

class RenewalsDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_renewals_data(renewalsid)

    renewalsQuery = "select t48, "+
						"status, "+
						"updated_ip, "+
						"added_ip, "+
						"to_char(adjustment_date,'mm/dd/yyyy'), "+
						"adjustmentid, "+
						"to_char(invoice_date,'mm/dd/yyyy'), "+
						"invoiceid, "+
						"to_char(startservice,'mm/dd/yyyy'), "+
						"updated_csr, "+
						"to_char(updated,'mm/dd/yyyy'), "+
						"added_csr, "+
						"to_char(added,'mm/dd/yyyy'), "+
						"csr, "+
						"error_type, "+
						"to_char(email_sent,'mm/dd/yyyy'), "+
						"to_char(claim_date,'mm/dd/yyyy'), "+
						"claim_rsubscriberid, "+
						"claim_accountid, "+
						"claim_code, "+
						"claimed, "+
						"months, "+
						"emailaddressid, "+
						"productcodeid, "+
						"orderdetailid, "+
						"orderid, "+
						"accountid, "+
						"renewalsid, "+
						"prev_endservice, "+
						"prev_productcodeid "+
                    "from ymax.renewals where renewalsid = #{renewalsid}"
        
        r = @conn.exec(renewalsQuery)
		
		@renewalsData = r.fetch()
		
		return @renewalsData
                    
    end
                    
	def get_t48
	  return @renewalsData[0]
	end
			
	def get_status
	  return @renewalsData[1]
	end
			
	def get_updated_ip
	  return @renewalsData[2]
	end
			
	def get_added_ip
	  return @renewalsData[3]
	end
			
	def get_adjustment_date
	  return @renewalsData[4]
	end
			
	def get_adjustmentid
	  return @renewalsData[5]
	end
			
	def get_invoice_date
	  return @renewalsData[6]
	end
			
	def get_invoiceid
	  return @renewalsData[7]
	end
			
	def get_startservice
	  return @renewalsData[8]
	end
			
	def get_updated_csr
	  return @renewalsData[9]
	end
			
	def get_updated
	  return @renewalsData[10]
	end
			
	def get_added_csr
	  return @renewalsData[11]
	end
			
	def get_added
	  return @renewalsData[12]
	end
			
	def get_csr
	  return @renewalsData[13]
	end
			
	def get_error_type
	  return @renewalsData[14]
	end
			
	def get_email_sent
	  return @renewalsData[15]
	end	
			
	def get_claim_date
	  return @renewalsData[16]
	end

	def get_claim_rsubscriberid
	  return @renewalsData[17]
	end
			
	def get_claim_accountid
	  return @renewalsData[18]
	end
			
	def get_claim_code
	  return @renewalsData[19]
	end
			
	def get_claimed
	  return @renewalsData[20]
	end
			
	def get_months
	  return @renewalsData[21]
	end
			
	def get_emailaddressid
	  return @renewalsData[22]
	end
			
	def get_productcodeid
	  return @renewalsData[23]
	end
			
	def get_orderdetailid
	  return @renewalsData[24]
	end
			
	def get_orderid
	  return @renewalsData[25]
	end
			
	def get_accountid
	  return @renewalsData[26]
	end
			
	def get_renewalsid
	  return @renewalsData[27]
	end
			
	def get_prev_endservice
	  return @renewalsData[28]
	end
			
	def get_prev_productcodeid
	  return @renewalsData[29]
	end
			
    def closeDBConnection		
		@conn.logoff
	end
end
    