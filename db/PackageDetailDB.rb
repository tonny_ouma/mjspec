require 'oci8'
require_relative 'DBConn.rb'

class PackageDetailDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_package_detail_data(packagedetailid)

    packagedetailQuery = "select serial_no, "+
								"rmadetailid, "+
								"returned, "+
								"active, "+
								"updated_ip, "+
								"added_ip, "+
								"updated_csr, "+
								"to_char(updated,'mm/dd/yyyy'), "+
								"added_csr, "+
								"to_char(added,'mm/dd/yyyy'), "+
								"orderdeliveryid, "+
								"orderid, "+
								"packagedetailid "+
							"from ymax.packagedetail where packagedetailid = #{packagedetailid}"
        
        
        r = @conn.exec(packagedetailQuery)
		
		@packagedetailData = r.fetch()
		
		return @packagedetailData
                    
	end
                    
	def get_serial_no
	  return @packagedetailData[0]
	end
			
	def get_rmadetailid
	  return @packagedetailData[1]
	end
			
	def get_returned
	  return @packagedetailData[2]
	end
			
	def get_active
	  return @packagedetailData[3]
	end
			
	def get_updated_ip
	  return @packagedetailData[4]
	end
			
	def get_added_ip
	  return @packagedetailData[5]
	end
			
	def get_updated_csr
	  return @packagedetailData[6]
	end
			
	def get_updated
	  return @packagedetailData[7]
	end
			
	def get_added_csr
	  return @packagedetailData[8]
	end
			
	def get_added
	  return @packagedetailData[9]
	end
			
	def get_orderdeliveryid
	  return @packagedetailData[10]
	end
			
	def get_orderid
	  return @packagedetailData[11]
	end
			
	def get_packagedetailid
	  return @packagedetailData[12]
	end
			
	def closeDBConnection		
		@conn.logoff
	end
end
    