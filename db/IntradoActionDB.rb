require 'oci8'
require_relative 'DBConn.rb'

class IntradoActionDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)	
	end	

	def set_intrado_action_data(intrado_actionid)
		
		intradoactionQuery ="select intrado_actionid," +
			  				" action," +                
			  				" arg," +                   
  			  				" accountid," +             
			  				" calling_party_no," +      
			  				" main_telephone_no," +     
			  				" customer_name," +         
			  				" house_no," +              
			  				" house_no_suffix," +       
			  				" prefix_directional," +    
			  				" street_name," +           
			  				" post_directional," +      
			  				" street_name_suffix," +    
			  				" location," +              
			  				" msag_community_name," +   
			  				" state," +                 
			  				" zip_code," +              
			  				" class_of_service," +      
			  				" type_of_service," +       
			  				" dial_tone_company_id," +  
							" to_char(completion_date,'mm/dd/yyyy')," + 			  				
			  				" country_id," +            
			  				" customer_code," +         
			  				" emergency_service_no," +  
			  				" exchange," +              
			  				" tar_code," +              
			  				" order_no," +              
			  				" error_type," +            
			  				" suberror_type," +
			  				" to_char(added,'mm/dd/yyyy')," + 			  				            
			  				" added_csr," +             
							" to_char(updated,'mm/dd/yyyy')," + 			  				
			  				" updated_csr," +           
			  				" to_char(locationid)," +            
			  				" added_ip," +              
			  				" updated_ip," +            
			  				" cpnid," +                    
			  				" response_code1," +        
			  				" to_char(endpointid)," +            
			  				" country" +               
				" from ymax.intrado_action" +
				" where intrado_actionid = #{intrado_actionid}"
				
		i = @conn.exec(intradoactionQuery)
		
		@intradoactionData = i.fetch()
		
		return @intradoactionData
	end
	
	def get_intrado_actionid
		return @intradoactionData[0]
	end


	def get_action
		return @intradoactionData[1]
	end
                
	def get_arg
		return @intradoactionData[2]
	end
                   
	def get_accountid
		return @intradoactionData[3]
	end
             
	def get_calling_party_no
		return @intradoactionData[4]
	end
      
	def get_main_telephone_no
		return @intradoactionData[5]
	end
     
	def get_customer_name
		return @intradoactionData[6]
	end
         
	def get_house_no
		return @intradoactionData[7]
	end

	def get_house_no_suffix
		return @intradoactionData[8]
	end
      
	def get_prefix_directional
		return @intradoactionData[9]
	end

	def get_street_name
		return @intradoactionData[10]
	end

	def get_post_directional
		return @intradoactionData[11]
	end

	def get_street_name_suffix
		return @intradoactionData[12]
	end

	def get_location
		return @intradoactionData[13]
	end

	def get_msag_community_name
		return @intradoactionData[14]
	end

	def get_state
		return @intradoactionData[15]
	end

	def get_zip_code
		return @intradoactionData[16]
	end

	def get_class_of_service
		return @intradoactionData[17]
	end

	def get_type_of_service
		return @intradoactionData[18]
	end

	def get_dial_tone_company_id
		return @intradoactionData[19]
	end

	def get_completion_date
		return @intradoactionData[20]
	end

	def get_country_id
		return @intradoactionData[21]
	end
   
	def get_customer_code
		return @intradoactionData[22]
	end

	def get_emergency_service_no
		return @intradoactionData[23]
	end

	def get_exchange
		return @intradoactionData[24]
	end

	def get_tar_code
		return @intradoactionData[25]
	end

	def get_order_no
		return @intradoactionData[26]
	end

	def get_error_type
		return @intradoactionData[27]
	end

	def get_suberror_type
		return @intradoactionData[28]
	end

	def get_added
		return @intradoactionData[29]
	end

	def get_added_csr
		return @intradoactionData[30]
	end

	def get_updated
		return @intradoactionData[31]
	end

	def get_updated_csr
		return @intradoactionData[32]
	end

	def get_locationid
		return @intradoactionData[33]
	end

	def get_added_ip
		return @intradoactionData[34]
	end

	def get_updated_ip
		return @intradoactionData[35]
	end

	def get_cpnid
		return @intradoactionData[36]
	end

	def get_response_code1
		return @intradoactionData[37]
	end

	def get_endpointid
		return @intradoactionData[38]
	end

	def get_country
		return @intradoactionData[39]
	end
 
    def closeDBConnection		
		@conn.logoff
	end
	
end