require 'oci8'
require_relative 'DBConn.rb'

class CpnDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_cpn_data(cpnid)
	
		cpnQuery =	"select cpnid," +
                    " cpn_type," +
					" accountid," +                     
					" calling_party_no," +
					" class_of_service," +
                    " type_of_service," +
                    " dial_tone_company_id," +
                    " to_char(trunc(completion_date),'mm/dd/yyyy')," +
                    " e911_status," +
                    " e911_tcsi_code," +
                    " to_char(trunc(e911_tcsi_date), 'mm/dd/yyyy')," + 
                    " to_char(trunc(e911_confirm), 'mm/dd/yyyy')," +
                    " order_no," +
                    " active," +
                    " to_char(trunc(order_date), 'mm/dd/yyyy')," +
                    " order_csr," +
                    " order_ipaddr," +
                    " order_tracking_no," +
                    " to_char(trunc(added), 'mm/dd/yyyy')," +
                    " added_csr," +
                    " to_char(trunc(updated), 'mm/dd/yyyy')," +
                    " updated_csr," +
                    " to_char(trunc(last_accessed), 'mm/dd/yyyy')," +
                    " to_char(trunc(outservice), 'mm/dd/yyyy')," +
                    " to_char(trunc(inservice), 'mm/dd/yyyy')," + 
                    " added_ip," +
                    " updated_ip," + 
                    " number_productcodeid," +
                    " emailaddressid," +
                    " state," + 
                    " country," +
                    " lata," +
                    " usagebits," +
                    " options," +
                    " to_char(trunc(autorenewdate), 'mm/dd/yyyy')," + 
                    " autorenew," +
                    " to_char(trunc(last_assigned_date), 'mm/dd/yyyy')," +
                    " nvl(renewalstate, '')," +
                    " endpointid," +
                    " nvl(forward_number, '')," +
                    " to_char(trunc(versiontime), 'mm/dd/yyyy')," + 
                    " to_char(nvl(subscriptionid, 0))" +
					" from ymax.cpn" +
					" where cpnid = #{cpnid}"
						
		r = @conn.exec(cpnQuery)
		
		@cpnData = r.fetch()
		
		return @cpnData
	 end
	
	 def get_cpnid
	 	return @cpnData[0]
	 end
	 
	 def get_cpn_type
	 	return @cpnData[1]
	 end
	 
	 def get_accountid
	 	return @cpnData[2]
	 end
	 
	 def get_calling_party_no
	 	return @cpnData[3]
	 end

	 def get_class_of_service
	 	return @cpnData[4]
	 end
	 
	 def get_type_of_service
	 	return @cpnData[5]
	 end

	 def get_dial_tone_company_id
	 	return @cpnData[6]
	 end
	 
	 def get_completion_date
	 	return @cpnData[7]
	 end

	 def get_e911_status
	 	return @cpnData[8]
	 end
	 
	 def get_e911_tcsi_code
	 	return @cpnData[9]
	 end
	 
	 def get_e911_tcsi_date
	 	return @cpnData[10]
	 end
	 
	 def get_e911_confirm
	 	return @cpnData[11]
	 end
	 
	 def get_order_no
	 	return @cpnData[12]
	 end
	 
	 def get_active
	 	return @cpnData[13]
	 end
	 
	 def get_order_date
	 	return @cpnData[14]
	 end
	 
	 def get_order_csr
	 	return @cpnData[15]
	 end
	 
	 def get_order_ipaddr
	 	return @cpnData[16]
	 end
	 
	 def get_order_tracking_no
	 	return @cpnData[17]
	 end
	 
	 def get_added
	 	return @cpnData[18]
	 end
	 
	 def get_added_csr
	 	return @cpnData[19]
	 end
	 
	 def get_updated
	 	return @cpnData[20]
	 end
	 
	 def get_updated_csr
	 	return @cpnData[21]
	 end
	 
	 def get_last_accessed
	 	return @cpnData[22]
	 end
	 
	 def get_outservice
	 	return @cpnData[23]
	 end
	 
	 def get_inservice
	 	return @cpnData[24]
	 end

	 def get_added_ip
	 	return @cpnData[25]
	 end
	 
	 def get_updated_ip
	 	return @cpnData[26]
	 end
	 
	 def get_number_productcodeid
	 	return @cpnData[27]
	 end
	 
	 def get_emailaddressid
	 	return @cpnData[28]
	 end
	 
	 def get_state
	 	return @cpnData[29]
	 end
	 
	 def get_country
	 	return @cpnData[30]
	 end
	 
	 def get_lata
	 	return @cpnData[31]
	 end
	 
	 def get_usagebits
	 	return @cpnData[32]
	 end
	 
	 def get_options
	 	return @cpnData[33]
	 end
	 
	 def get_autorenewdate
	 	return @cpnData[34]
	 end
	 
	 def get_autorenew
	 	return @cpnData[35]
	 end
	 
	 def get_last_assigned_date
	 	return @cpnData[36]
	 end
	 
	 def get_renewalstate
	 	return @cpnData[37]
	 end
	 
	 def get_endpointid
	 	return @cpnData[38]
	 end
	 
	 def get_forward_number
	 	return @cpnData[39]
	 end
	 
	 def get_versiontime
	 	return @cpnData[40]
	 end
	 
	 def get_versiontime
	 	return @cpnData[41]
	 end
	 
	 def get_subscriptionid
	 	return @cpnData[42]
	 end
	 
    def closeDBConnection		
		@conn.logoff
	end
	
end