require 'oci8'
require_relative 'DBConn.rb'

class LocationDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)
	end


	def get_locationids(accountid)
		 
		@locationIds = Array.new		 
		locationIdQuery = "select locationid from ymax.location where accountid = #{accountid}"		 
		@conn.exec(locationIdQuery) do |r| @locationIds.push(r.join(',')) end		  	    
	   return @locationIds		 
		 
	end
	
	def set_location_records(accountid)
		 
		 @locationRecords = Array.new
		 
		 locationRecsQuery = "select locationid," +
			  			" location_type," +
			  			" accountid," +             
						" first_name," +            
		  				" last_name," +             
		  				" dba," +                   
		  				" house_no," +              
			  			" house_no_suffix," +       
			  			" prefix_directional," +    
						" street_name," +           
		  				" post_directional," +      
		  				" street_name_suffix," +   
			  			" location," +              
			  			" msag_community_name," +   
						" state," +                 
		  				" zip_code," +              
		  				" active," +                
		  				" to_char(order_date,'mm/dd/yyyy')," + 			  				            
			  			" order_csr," +             
			  			" order_ipaddr," +          
			  			" order_tracking_no," +     
						" to_char(added,'mm/dd/yyyy')," + 			  				                 
		  				" added_csr," +             
		  				" to_char(updated,'mm/dd/yyyy')," + 			  				               
		  				" updated_csr," +           
			  			" tv_type," +               
			  			" net_type," +             
						" country_id," +            
		  				" country," +               
		  				" customer_code," +         
			  			" emergency_service_no," +  
			  			" exchange," +              
			  			" tar_code," +              
						" phone," +                
		  				" geocode," +              
		  				" added_ip," +             
						" updated_ip," +           
						" incity," +               
						" inlocal," +              
		  				" emailaddressid" +       
				" from ymax.location" +
				" where accountid = #{accountid}"
 				 
		 @conn.exec(locationRecsQuery) do |r| @locationRecords.push(r.join(',')) end		  	    
		
	   return @locationRecords		 
		 
	end

	def set_location_data(locationid)

		locationQuery = "select locationid," +
			  			" location_type," +
			  			" accountid," +             
						" first_name," +            
		  				" last_name," +             
		  				" dba," +                   
		  				" house_no," +              
			  			" house_no_suffix," +       
			  			" prefix_directional," +    
						" street_name," +           
		  				" post_directional," +      
		  				" street_name_suffix," +   
			  			" location," +              
			  			" msag_community_name," +   
						" state," +                 
		  				" zip_code," +              
		  				" active," +                
		  				" to_char(order_date,'mm/dd/yyyy')," + 			  				            
			  			" order_csr," +             
			  			" order_ipaddr," +          
			  			" order_tracking_no," +     
						" to_char(added,'mm/dd/yyyy')," + 			  				                 
		  				" added_csr," +             
		  				" to_char(updated,'mm/dd/yyyy')," + 			  				               
		  				" updated_csr," +           
			  			" tv_type," +               
			  			" net_type," +             
						" country_id," +            
		  				" country," +               
		  				" customer_code," +         
			  			" emergency_service_no," +  
			  			" exchange," +              
			  			" tar_code," +              
						" phone," +                
		  				" geocode," +              
		  				" added_ip," +             
						" updated_ip," +           
						" incity," +               
						" inlocal," +              
		  				" emailaddressid" +       
				" from ymax.location" +
				" where locationid = #{locationid}"
				
		l = @conn.exec(locationQuery)
		
		@locationData = l.fetch()
		
		return @locationData
	end
	
	def get_locationid            
		return @locationData[0]
	end

	def get_location_type         
		return @locationData[1]
	end

	def get_accountid             
		return @locationData[2]
	end

	def get_first_name            
		return @locationData[3]
	end

	def get_last_name             
		return @locationData[4]
	end

	def get_dba                   
		return @locationData[5]
	end

	def get_house_no              
		return @locationData[6]
	end

	def get_house_no_suffix       
		return @locationData[7]
	end

	def get_prefix_directional    
		return @locationData[8]
	end

	def get_street_name           
		return @locationData[9]
	end

	def get_post_directional      
		return @locationData[10]
	end

	def get_street_name_suffix    
		return @locationData[11]
	end

	def get_location              
		return @locationData[12]
	end

	def get_msag_community_name   
		return @locationData[13]
	end

	def get_state                 
		return @locationData[14]
	end

	def get_zip_code              
		return @locationData[15]
	end

	def get_active                
		return @locationData[16]
	end

	def get_order_date            
		return @locationData[17]
	end

	def get_order_csr             
		return @locationData[18]
	end

	def get_order_ipaddr          
		return @locationData[19]
	end

	def get_order_tracking_no     
		return @locationData[20]
	end

	def get_added                 
		return @locationData[21]
	end

	def get_added_csr             
		return @locationData[22]
	end

	def get_updated               
		return @locationData[23]
	end

	def get_updated_csr           
		return @locationData[24]
	end

	def get_tv_type               
		return @locationData[25]
	end

	def get_net_type              
		return @locationData[26]
	end

	def get_country_id            
		return @locationData[27]
	end

	def get_country               
		return @locationData[28]
	end

	def get_customer_code         
		return @locationData[29]
	end

	def get_emergency_service_no  
		return @locationData[30]
	end

	def get_exchange              
		return @locationData[31]
	end

	def get_tar_code              
		return @locationData[32]
	end

	def get_phone                
		return @locationData[33]
	end

	def get_geocode              
		return @locationData[34]
	end

	def get_added_ip             
		return @locationData[35]
	end

	def get_updated_ip           
		return @locationData[36]
	end

	def get_incity               
		return @locationData[37]
	end

	def get_inlocal              
		return @locationData[38]
	end

	def get_emailaddressid
		return @locationData[39]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end