require 'oci8'
require_relative 'DBConn.rb'

class PinlessDetailDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_pinless_detail_data(pinlessdetailid)

    pinlessdetailQuery = "select cpnid, "+
								"updated_ip, "+
								"updated_csr, "+
								"to_char(updated,'mm/dd/yyyy'), "+
								"added_ip, "+
								"added_csr, "+
								"to_char(added,'mm/dd/yyyy'), "+
								"active, "+
								"to_char(outservice,'mm/dd/yyyy'), "+
								"to_char(inservice,'mm/dd/yyyy'), "+
								"announce, "+
								"descr, "+
								"type, "+
								"ani, "+
								"accountid, "+
								"pinlessid, "+
								"pinlessdetailid "+
							"from ymax.pinlessdetail where pinlessdetailid = #{pinlessdetailid}"
        
        r = @conn.exec(pinlessdetailQuery)
		
		@pinlessdetailData = r.fetch()
		
		return @pinlessdetailData
                    
	end
                    
	def get_cpnid
	  return @pinlessdetailData[0]
	end
				
	def get_updated_ip
		return @pinlessdetailData[1]
	end
				
	def get_updated_csr
		return @pinlessdetailData[2]
	end
				
	def get_updated
		return @pinlessdetailData[3]
	end
				
	def get_added_ip
		return @pinlessdetailData[4]
	end
				
	def get_added_csr
		return @pinlessdetailData[5]
	end
				
	def get_added
		return @pinlessdetailData[6]
	end
				
	def get_active
		return @pinlessdetailData[7]
	end
				
	def get_outservice
		return @pinlessdetailData[8]
	end
				
	def get_inservice
		return @pinlessdetailData[9]
	end
				
	def get_announce
		return @pinlessdetailData[10]
	end
				
	def get_descr
		return @pinlessdetailData[11]
	end
				
	def get_type
		return @pinlessdetailData[12]
	end
				
	def get_ani
		return @pinlessdetailData[13]
	end
				
	def get_accountid
		return @pinlessdetailData[14]
	end
				
	def get_pinlessid
		return @pinlessdetailData[15]
	end
				
	def get_pinlessdetailid
		return @pinlessdetailData[16]
	end
			
	def closeDBConnection		
		@conn.logoff
	end
end
    