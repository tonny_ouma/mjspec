require 'oci8'
require_relative 'DBConn.rb'

class OrderKeyDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
	end	

	def set_order_key_data(orderkeyid)

		orderkeyQuery = "select orderkeyid," +
								" orderid," +
								" orderkey," +
								" order_number," + 
								" to_char(trunc(shipdate), 'mm/dd/yyyy')," +
								" actionid," +
								" to_char(trunc(emaildate), 'mm/dd/yyyy')," +
								" to_char(trunc(expiredate), 'mm/dd/yyyy')," + 
								" claimed," +
								" to_char(trunc(claim_date), 'mm/dd/yyyy')," +
								" inbound_orderid," +
								" csr," +
								" to_char(trunc(added), 'mm/dd/yyyy')," +
								" added_csr," +
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," + 
								" added_ip," + 
								" updated_ip," +
								" giftjack_orderid," +
								" productcodeid" +
						" from  ymax.orderkey" +
						" where orderkeyid = #{orderkeyid}"
				
		r = @conn.exec(orderkeyQuery)
		
		@orderkeyData = r.fetch()
		
		return @orderkeyData
	 end

	def get_orderkeyid 
	   return @orderkeyData[0]
	end
	
	def get_orderid
	   return @orderkeyData[1]
	end
	
	def get_orderkey
	   return @orderkeyData[2]
	end
	
	def get_order_number
	   return @orderkeyData[3]
	end

	def get_shipdate
	   return @orderkeyData[4]
	end
	
	def get_actionid
	   return @orderkeyData[5]
	end

	def get_emaildate
	   return @orderkeyData[6]
	end
	
	def get_expiredate
	   return @orderkeyData[7]
	end

	def get_claimed
	   return @orderkeyData[8]
	end
	
	def get_claim_date
	   return @orderkeyData[9]
	end

	def get_inbound_orderid
	   return @orderkeyData[10]
	end
	
	def get_csr
	   return @orderkeyData[11]
	end
	
	def get_added
	   return @orderkeyData[12]
	end

	def get_added_csr
	   return @orderkeyData[13]
	end
	
	def get_updated
	   return @orderkeyData[14]
	end
	
	def get_updated_csr
	   return @orderkeyData[15]
	end
	
	def get_added_ip
	   return @orderkeyData[16]
	end
	
	def get_updated_ip
	   return @orderkeyData[17]
	end
	
	def get_giftjack_orderid
	   return @orderkeyData[18]
	end
	
	def get_productcodeid
	   return @orderkeyData[19]
	end

    def closeDBConnection		
		@conn.logoff
	end
end
