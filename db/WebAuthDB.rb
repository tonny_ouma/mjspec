require 'oci8'
require_relative 'DBConn.rb'

class WebAuthDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_webauth_data(userid, groupid)
		webauthQuery =	"select userid," +			  	
			   		  	" groupid," +           
			  		  	" to_char(added,'mm/dd/yyyy')," + 
			  		  	" added_csr," +      
			  		  	" added_ip," +          
			  		  	" to_char(updated,'mm/dd/yyyy')," +           
			  		  	" updated_csr," + 
			  		  	" updated_ip" +  
					  	" from ymax.webauth" +
					  	" where userid = #{userid}"
					  	" and groupid = #{groupid}"

		w = @conn.exec(webauthQuery)
		
		@webauthData = w.fetch()
		
		return @webauthData
	end
	
	def get_userid
		return @webauthData[0]
	end

	def get_groupid           
		return @webauthData[1]
	end

	def get_added 
		return @webauthData[2]
	end

	def get_added_csr      
		return @webauthData[3]
	end

	def get_added_ip          
		return @webauthData[4]
	end

	def get_updated           
		return @webauthData[5]
	end

	def get_updated_csr 
		return @webauthData[6]
	end

	def get_updated_ip  	
		return @webauthData[7]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end