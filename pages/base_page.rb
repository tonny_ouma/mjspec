# filename: base_page.rb
 require 'selenium-webdriver'
 

   DEVBAR	 		 =  { id: 'divDevelopBtn' }
   PUSHABLE 		 =  { id: 'selModePage' }
   DEVFRAME          =  { id: 'ifDevelopContent' }
   DEVHIDE         	 =  { css:'a[title="Hide Developer Bar"]' }
   
class BasePage   

  def initialize(driver)
      @driver = driver
  end
  
  def visit(url)
     @driver.get url
  end
  
   def find(locator)
     @driver.find_element locator
  end
  
  def type(text, locator)
     find(locator).send_keys text
  end
  
  def click(locator)
    find(locator).click
  end
  
  def selectByText(locator,txt)
    Selenium::WebDriver::Support::Select.new(find(locator)).select_by(:text, txt)
  end
  
  def selectByIndex(locator,num)
    Selenium::WebDriver::Support::Select.new(find(locator)).select_by(:index, num)
       
  end
  
  def getSerNum(locator)
     elem=Selenium::WebDriver::Support::Select.new(find(locator)).first_selected_option
     return elem.text
  end 
      
  def is_displayed?(locator)
    find(locator).displayed?
  end
  
  def switchWindow
        wnd_titl = @driver.window_handles.map do |w|
	  		@driver.switch_to.window(w)
	  		[w,@driver.title]
	  		
		end
  end
  
  def closeWindow        
	  @driver.close	  		
  end
  
  def switchFrame(locator)
        @driver.switch_to().frame(find locator) 
  end
  
  def wait_for(seconds)
        Selenium::WebDriver::Wait.new(timeout: seconds).until { yield }
  end
  
  def getText(locator)
  	find(locator).text
  end 
  
  def setPushable
      wait_for(5){click DEVBAR}
      wait_for(5){switchFrame DEVFRAME}
      wait_for(5){selectByText PUSHABLE, 'Pushable Pages'}
      wait_for(5){switchWindow}
      wait_for(5){click DEVHIDE}      
  end
 
  
end
