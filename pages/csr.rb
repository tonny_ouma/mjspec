require_relative 'base_page.rb'
require_relative '../db/RegDB.rb'

class Csr < BasePage
	
    CSRLOGIN     =  { id: 'btnLogin' }
    ACCOUNTSTAB  =  { link: 'Accounts' }
    JACKTYPE  =  { id: 'selSerialType' }
    SERIALNUM =  { id: 'selSerial' }
    
    CREATEACCOUNT =  { css: '.col-b>button.btn' }
    
    FIRSTNAME    	 =  { id: 'first_name' }
    LASTNAME     	 =  { id: 'last_name' }
    STREETNAME   	 =  { id: 'street_name' }
    CITY         	 =  { id: 'msag_community_name'}
    STATE        	 =  { id: 'activation:location[-3].state'}
    ZIP          	 =  { id: 'zip_code'}
    EMAIL        	 =  { id: 'conf-email'}
    EMAILVERIFY 	 =  { id: 'email-verify'}
    PASSWORD     	 =  { id: 'password'}
    PASSWORDVERIFY   =  { id: 'password-verify'}
    CREATEACCOUNT2	 =	{ xpath: "(//button[@type='button'])[2]"}
    
    DEVICENAME		 =	{ id: 'device-name'}
    EMAILMETHOD		 =	{ id: 'emailMethod'}
    SENDMYCODE		 =	{ css: 'button[type=submit]'}
    
    ACTCODE			 =	{ id: 'code1'}
    ACTIVATEMYDEVICE =	{ css: 'button[type=submit]'}
    
    TERMSACCEPT		 =	{ id: 'terms-accept'}
    CONTINUE 		 =	{ css: 'button[type=submit]'}
    
    CPNVANITY        =  { xpath: ".//*[@id='activation-number']/div/div[2]/a[3]/div/p"}
    CPNCANADIAN      =  { xpath: ".//*[@id='activation-number']/div/div[2]/a[2]/div/p"}
    CPNSEARCVAN      =  { id: 'search-vanity'}
    CPNCHOOSEDIGITS  =  { id: 'choose-digits'}
    CPNCUSTOMNUM     =  { id: 'freenumber'}
    CPNSTATE         =  { id: 'activation:cpn[-1].state'}
    CPNAREACODE      =  { id: 'activation:cpn[-1].areacode'}
    CPNAREAEXCHANGE  =  { id: 'activation:cpn[-1].exchange'}
    CPNCALLINGPN     =  { id: 'activation:cpn[-1].calling_party_no'}
    CPNSEARCHBTN     =  { id: 'btnSubmit'}
    CPNADDTOCART     =  { id: 'btnSubmit'}
    
    E911LOCATION     =  { id: 'activation:e911locationid'}
    E911CONFIRMLOC   =  { xpath: ".//*[@id='activation-911']/div/div[3]/button"}
    E911CONFIRM      =  { name: 'confirm'}
    E911INCITY       =  { name: 'incity'}
    E911CONFIRMCITY  =  { xpath: ".//*[@id='modalPopup']/div/center/div[2]/button"}
    SKIPFORNOW       =  { css: 'button[type=submit]'}
    TAKEIPP5         =  { id: 'sel-credit-a'}
    PROCEEDTOCHECKOUT=  { xpath: ".//*[@id='activation-number']/div/div[4]/div[2]/button[2]"}
    
    CCLOCATION       =  { id: 'activation:billinglocationid'}
    CARDNUMBER       =  { id: 'cardnumber'}
    CVV2             =  { id: 'cvv2'}
    CCEXPMONTH       =  { id: 'activation:creditcard[-1].month'}
    CCEXPYEAR        =  { id: 'year'}
    REVIEWORDER      =  { xpath: ".//*[@id='cart']/section/div/div[2]/div[2]/button"}
    PLACEORDER       =  { xpath: ".//*[@id='cart']/section/div/div/div[8]/div[2]/button"}
    
    EXITBTNLOWER     =  { id: "btnExit"}
    CLOSEREG         =  { id: "aCloser"}
    
    SUCCESSMESSAGE   =  { class: "cartdetail-item"}
    
    CSRDEVICEEXP =  {xpath: ".//*[@id='divMainLayoutPrintContent']/table/tbody/tr/td/table/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr/td/table[1]/tbody/tr/td[1]/table/tbody/tr[16]/td"}

    
    def initialize(driver)       
	    super
	    visit 'http://steve.schneider:Er!n2016@develop.talk4free.com/csr/login.html'	      
    end
    
    def setRegWorked(boolean)
    	@regWorked = boolean
    end
    
    def getRegWorked
    	return @regWorked
    end 
 
    def getExpDate
    	return getText(CSRDEVICEEXP)
    end
     
        
  def check_csr_with(jack_type,fname, numType)

 	 visit 'http://develop.talk4free.com/csr/login.html'
 	 sleep(1)
 	 type 'steve.schneider', CSRNAME
 	 type 'Er!n2016', CSRPASSWORD 
 	 click CSRLOGIN
 	 sleep(1)
	 click ACCOUNTSTAB
	 sleep(1)
	 type  fname + " SCHNEIDER", CSRLOOKUP
	 click CSRSEARCH
	 click CSRDEVICETAB 

 end

end