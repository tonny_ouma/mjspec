require_relative '../db/OrderDeliveryDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = OrderDeliveryDB.new
valDB.set_orders_delivery_data('444463')

p 'ORDERDELIVERYID'
p valDB.get_orderdeliveryid

p 'ORDERID'
p valDB.get_orderid

p 'ACCOUNTID'
p valDB.get_accountid

p 'LOCATIONID' 
p valDB.get_locationid

p 'BATCH_NO'
p valDB.get_batch_no

p 'SHIPSTATUS'
p valDB.get_shipstatus

p 'SHIPDATE' 
p valDB.get_shipdate

p 'TOTAL_QTY'
p valDB.get_total_qty

p 'TOTAL_WEIGHT'
p valDB.get_total_weight

p 'MAILCLASS'
p valDB.get_mailclass

p 'TORETURNCODE'
p valDB.get_toreturncode

p 'STATUS'
p valDB.get_status

p 'USPS_TRACKINGNO'
p valDB.get_usps_trackingno

p 'FINAL_POSTAGE'
p valDB.get_final_postage

p 'TRANSACTIONID'
p valDB.get_transactionid

p 'TRANSACTION_DATETIME'
p valDB.get_transaction_datetime

p 'POSTMARK_DATE'
p valDB.get_postmark_date

p 'DELIVERDATE' 
p valDB.get_deliverdate

p 'PRINT_COUNT' 
p valDB.get_print_count

p 'CSR'
p valDB.get_csr

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'UPDATED' 
p valDB.get_updated

p 'UPDATED_CSR' 
p valDB.get_updated_csr

p 'RESHIPPED'
p valDB.get_reshipped

p 'BARCODE1'
p valDB.get_barcode1

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'DELIVERYCATEGORY'
p valDB.get_deliverycategory

p 'PACKAGE_COUNT'
p valDB.get_package_count

p 'DELIVERYCOSTCONTAINERID'
p valDB.get_deliverycostcontainerid

p 'SCANNED' 
p valDB.get_scanned

p 'RESHIP_ORDERDELIVERYID'
p valDB.get_reship_orderdeliveryid

p 'RUSH'
p valDB.get_rush

p 'ORDER_DATE'
p valDB.get_order_date


valDB.closeDBConnection

