require_relative '../db/PremiumDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = PremiumDB.new
valDB.set_premium_data('499792')

p 'PREMIUMID'
p valDB.get_premiumid

p 'ACCOUNTID'
p valDB.get_accountid

p 'ORDERID'
p valDB.get_orderid

p 'ORDERDETAILID'
p valDB.get_orderdetailid

p 'PRODUCTCODEID'
p valDB.get_productcodeid

p 'EMAILADDRESSID'
p valDB.get_emailaddressid

p 'QUANTITY'
p valDB.get_quantity

p 'ERROR_TYPE'
p valDB.get_error_type

p 'CLAIMED'
p valDB.get_claimed

p 'CLAIM_CODE'
p valDB.get_claim_code

p 'CLAIM_ACCOUNTID'
p valDB.get_claim_accountid

p 'CLAIM_CPNID'
p valDB.get_claim_cpnid

p 'CLAIM_DATE'
p valDB.get_claim_date

p 'INSERVICE'
p valDB.get_inservice

p 'INVOICEID'
p valDB.get_invoiceid

p 'INVOICE_DATE'
p valDB.get_invoice_date

p 'ADJUSTMENTID'
p valDB.get_adjustmentid

p 'ADJUSTMENT_DATE'
p valDB.get_adjustment_date

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

valDB.closeDBConnection

