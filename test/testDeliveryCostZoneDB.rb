require_relative '../db/DeliveryCostZoneDB.rb'
require "rspec"
require "date"

valDB = DeliveryCostZoneDB.new
valDB.set_delivery_cost_zone_data('168')

p 'get_deliverycostzoneid'
p valDB.get_deliverycostzoneid

p 'get_active'
p valDB.get_active

p 'get_added' 
p valDB.get_added

p 'get_added_csr'      
p valDB.get_added_csr

p 'get_added_ip'          
p valDB.get_added_ip

p 'get_country'           
p valDB.get_country

p 'get_deliverycategory'
p valDB.get_deliverycategory

p 'get_deliverycostcontainerid'
p valDB.get_deliverycostcontainerid

p 'get_fromzone'       
p valDB.get_fromzone

p 'get_tozone'              
p valDB.get_tozone

p 'get_updated'           
p valDB.get_updated

p 'get_updated_csr' 
p valDB.get_updated_csr

p 'get_updated_ip'
p valDB.get_updated_ip


valDB.closeDBConnection
