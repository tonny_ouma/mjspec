require_relative '../db/DistributionDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = DistributionDB.new
valDB.set_distribution_data('6855405')

p 'DISTRIBUTIONID'
p valDB.get_distributionid

p 'INVOICEID'
p valDB.get_invoiceid

p 'ACCOUNTID'
p valDB.get_accountid

p 'ORDERID'
p valDB.get_orderid

p 'REFERENCEID'
p valDB.get_referenceid

p 'INVOICE_DATE'
p valDB.get_invoice_date

p 'CATEGORY'
p valDB.get_category

p 'AMOUNT'
p valDB.get_amount

p 'GLACCOUNT'
p valDB.get_glaccount

p 'CSR'
p valDB.get_csr

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'ACTIVE'
p valDB.get_active

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_IP'
p valDB.get_updated_ip

valDB.closeDBConnection

