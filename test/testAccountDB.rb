require_relative '../db/AccountDB.rb'
require "rspec"
require "date"


valDB = AccountDB.new
valDB.set_account_data('SLS-2016-07-20-11-11-53')

p 'get_accountid'
p valDB.get_accountid
	 
p 'get_locationid'
p valDB.get_locationid

p 'get_emailaddressid'
p valDB.get_emailaddressid

p 'get_account_type'
p valDB.get_account_type

p 'get_busres'
p valDB.get_busres

p 'get_get_customer_name'
p valDB.get_customer_name

p 'get_balance'
p valDB.get_balance

p 'get_payment_due'
p valDB.get_payment_due

p 'get_invoices'
p valDB.get_invoices

p 'get_last_invoice'
p valDB.get_last_invoice

p 'get_payments'
p valDB.get_payments

p 'get_last_payment'
p valDB.get_last_payment

p 'get_refunds'
p valDB.get_refunds 

p 'get_last_refund'
p valDB.get_last_refund

p 'get_debits'
p valDB.get_debits
	 
p 'get_last_debit'
p valDB.get_debits

p 'get_credits'
p valDB.get_credits

p 'get_last_credit'
p valDB.get_last_credit

p 'get_cpni_flg'
p valDB.get_cpni_flg

p 'get_active'
p valDB.get_active

p 'get_order_date'
p valDB.get_order_date

p 'get_order_csr'
p valDB.get_order_csr

p 'get_order_ipaddr'
p valDB.get_order_ipaddr

p 'get_order_tracking_no'
p valDB.get_order_tracking_no

p 'get_csr'
p valDB.get_csr

p 'get_added'
p valDB.get_added

p 'get_added_csr'
p valDB.get_added_csr	 

p 'get_updated'
p valDB.get_updated

p 'get_updated_csr'
p valDB.get_updated_csr

p 'get_fraud_account'
p valDB.get_fraud_account

p 'get_added_ip'
p valDB.get_added_ip

p 'get_updated_ip'
p valDB.get_updated_ip 

p 'get_sip_iid'
p valDB.get_sip_iid

p 'get_ask_replace'
p valDB.get_ask_replace 	 

p 'get_hu'
p valDB.get_hu

p 'get_referrer_accountid'
p valDB.get_referrer_accountid

p 'get_inbound_referrerdetailid'
p valDB.get_inbound_referrerdetailid

p 'get_loyal'
p valDB.get_loyal

p 'get_chargebacks'
p valDB.get_chargebacks

p 'get_nflags'
p valDB.get_nflags

p 'get_nocontact'
p valDB.get_nocontact

p 'get_treatments'
p valDB.get_treatments    

p 'get_vip'
p valDB.get_vip

p 'get_first_name'
p valDB.get_first_name

p 'get_last_name'
p valDB.get_last_name  

p 'get_website'
p valDB.get_website 	 

valDB.closeDBConnection

