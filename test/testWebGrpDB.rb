require_relative '../db/WebGrpDB.rb'
require "rspec"
require "date"

valDB = WebGrpDB.new
valDB.set_webgrp_data('1299')


p 'groupid'           
p valDB.get_groupid

p 'name'
p valDB.get_name

p 'description'
p valDB.get_description

p 'added' 
p valDB.get_added

p 'added_csr'      
p valDB.get_added_csr

p 'added_ip'          
p valDB.get_added_ip

p 'updated'           
p valDB.get_updated

p 'updated_csr' 
p valDB.get_updated_csr

p 'updated_ip'  
p valDB.get_updated_ip

valDB.closeDBConnection




