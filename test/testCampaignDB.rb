require_relative '../db/CampaignDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = CampaignDB.new
valDB.set_campaign_data('282')

p 'CAMPAIGNID'
p valDB.get_campaignid

p 'CAMPAIGNCODE'
p valDB.get_campaigncode

p 'CATEGORY'
p valDB.get_category

p 'DESCRIPTION'
p valDB.get_description

p 'PROMOSALE'
p valDB.get_promosale

p 'PIECES_OUT'
p valDB.get_pieces_out

p 'ORDERS_IN'
p valDB.get_orders_in

p 'ACTIVE'
p valDB.get_active

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'CAMPAIGN_URL'
p valDB.get_campaign_url

p 'GIVE_MAGICDOLLARS'
p valDB.get_give_magicdollars

p 'LANDING_PAGE'
p valDB.get_landing_page

valDB.closeDBConnection

