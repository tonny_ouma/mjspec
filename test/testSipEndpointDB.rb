require_relative '../db/SipEndpointDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = SipEndpointDB.new
valDB.set_sip_endpoint_data('E999997323401')
	 
p 'sip_alias'   
p valDB.get_sip_alias

p 'admin_state' 
p valDB.get_admin_state

p 'alerttimer'      
p valDB.get_alerttimer

p 'ann_id'              
p valDB.get_ann_id

p 'app_token'      
p valDB.get_app_token

p 'app_type'          
p valDB.get_app_type

p 'app_ver'            
p valDB.get_app_ver

p 'cap'       
p valDB.get_cap

p 'data_status' 
p valDB.get_data_status

p 'data_version'              
p valDB.get_data_version

p 'dev_type'          
p valDB.get_dev_type

p 'entity_id'          
p valDB.get_entity_id

p 'feature_set'   
p valDB.get_feature_set

p 'iid'         
p valDB.get_iid

p 'info'     
p valDB.get_info

p 'last_modified'              
p valDB.get_last_modified

p 'last_state_updated'
p valDB.get_last_state_updated

p 'last_transact'             
p valDB.get_last_transact

p 'last_updated'               
p valDB.get_last_updated

p 'mac_address'
p valDB.get_mac_address

p 'modified_date'             
p valDB.get_modified_date

p 'nortpbp'           
p valDB.get_nortpbp

p 'op_state'          
p valDB.get_op_state

p 'orig_sip_alias'             
p valDB.get_orig_sip_alias

p 'orig_sip_ip'     
p valDB.get_orig_sip_ip

p 'orig_sip_port'              
p valDB.get_orig_sip_port

p 'private_partition'     
p valDB.get_private_partition

p 'pserver_id'      
p valDB.get_pserver_id

p 'quota'
p valDB.get_quota

p 'read_level'      
p valDB.get_read_level

p 'reserved'          
p valDB.get_reserved

p 'rg_id'  
p valDB.get_rg_id

p 'sip_encryption'            
p valDB.get_sip_encryption

p 'sip_extension'               
p valDB.get_sip_extension

p 'sip_ip'  
p valDB.get_sip_ip

p 'sip_port'
p valDB.get_sip_port

p 'sip_token'        
p valDB.get_sip_token

p 'sip_ttl'               
p valDB.get_sip_ttl

p 's_desc'
p valDB.get_s_desc

p 'telecode'           
p valDB.get_telecode

p 'user_domain'               
p valDB.get_user_domain

p 'vendor_id'      
p valDB.get_vendor_id

p 'visit_instance'             
p valDB.get_visit_instance

p 'write_level'    
p valDB.get_write_level
		

valDB.closeDBConnection