require_relative '../db/NotesDB.rb'
require "rspec"
require "date"

valDB = NotesDB.new
valDB.set_notes_data('6054931')

p 'notesid'
p valDB.get_notesid
p 'accountid'
p valDB.get_accountid
p 'cpnid'   
p valDB.get_cpnid       
p 'rsubscriberid'
p valDB.get_rsubscriberid
p 'note_type'
p valDB.get_note_type      
p 'notetext'
p valDB.get_notetext
p 'html'
p valDB.get_html
p 'webuserid'
p valDB.get_webuserid
p 'csr'     
p valDB.get_csr     
p 'added'
p valDB.get_added
p 'added_csr'
p valDB.get_added_csr
p 'updated'
p valDB.get_updated
p 'updated_csr'
p valDB.get_updated_csr
p 'note_date'
p valDB.get_note_date
p 'active'
p valDB.get_active
p 'added_ip'
p valDB.get_added_ip
p 'updated_ip'
p valDB.get_updated_ip
p 'ticket'
p valDB.get_ticket

valDB.closeDBConnection
