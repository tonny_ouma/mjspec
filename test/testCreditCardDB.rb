require_relative '../db/CreditCardDB.rb'
require "rspec"
require "date"


valDB = CreditCardDB.new
creditcardidPK = valDB.get_creditcardid_pk(11891935)
puts "creditcardidPK is #{creditcardidPK.to_i}"
valDB.set_credit_card_data(creditcardidPK)
	 
p 'get_creditcardid'
p valDB.get_creditcardid		
	 
p 'get_accountid'
p valDB.get_accountid		
	 
p 'get_full_name'
p valDB.get_full_name		
	 
p 'get_first_name'
p valDB.get_first_name		
	 
p 'get_last_name'
p valDB.get_last_name		
	 
p 'get_card_type'
p valDB.get_card_type		
	 
p 'get_exp_date'
p valDB.get_exp_date		
	 
p 'get_active'
p valDB.get_active		
	 
p 'get_csr'
p valDB.get_csr		
	 
p 'get_added'
p valDB.get_added		
	 
p 'get_added_csr'
p valDB.get_added_csr		
	 
p 'get_updated'
p valDB.get_updated		
	 
p 'get_updated_csr'
p valDB.get_updated_csr		
	 
p 'get_added_ip'
p valDB.get_added_ip		
	 
p 'get_updated_ip'
p valDB.get_updated_ip		
	 
p 'get_fraud_override'
p valDB.get_fraud_override
	 
p 'get_fraud_csr'
p valDB.get_fraud_csr		
	 
p 'get_cvv2_match'
p valDB.get_cvv2_match		
	 
p 'get_processor_avs'
p valDB.get_processor_avs		
	 
p 'get_addr_match'
p valDB.get_addr_match		
	 
p 'get_zip_match'
p valDB.get_zip_match		
	 
p 'get_debit'
p valDB.get_debit		
	 
p 'get_locationid'
p valDB.get_locationid		
	 
p 'get_fixed'
p valDB.get_fixed		
	 
p 'get_first_charge_date'
p valDB.get_first_charge_date
		
p 'get_charge_count'
p valDB.get_charge_count		
	 
p 'get_use_for_recurring'
p valDB.get_use_for_recurring		
	 
p 'get_route_no'
p valDB.get_route_no		
	 
p 'get_istoken'
p valDB.get_istoken		
	 
p 'get_mop'
p valDB.get_mop		

valDB.closeDBConnection