require_relative '../db/NsubscriberDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = NsubscriberDB.new
valDB.set_nsubscriber_data('517963')

p 'NSUBSCRIBERID'
p valDB.get_nsubscriberid

p 'ACTIVE'
p valDB.get_active

p 'DEVICE_TYPE'
p valDB.get_device_type

p 'SERIAL_NO'
p valDB.get_serial_no

p 'ADDED_SEQ'
p valDB.get_added_seq

p 'ENDPOINTID'
p valDB.get_endpointid

p 'STARTSERVICE'
p valDB.get_startservice

p 'ENDSERVICE'
p valDB.get_endservice

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'X_ACCOUNTID'
p valDB.get_x_accountid

p 'DEVICENAME'
p valDB.get_devicename

p 'PROVISIONINGDELAY'
p valDB.get_provisioningdelay

valDB.closeDBConnection

