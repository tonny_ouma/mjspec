require_relative '../db/DeviceInfoDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = DeviceInfoDB.new
valDB.set_device_info_data('345')

p 'DEVICEINFOID'
p valDB.get_deviceinfoid

p 'SERIAL_NO'
p valDB.get_serial_no

p 'USAGE_TYPE'
p valDB.get_usage_type

p 'DEVICE_TYPE'
p valDB.get_device_type

p 'DEVICE_HW'
p valDB.get_device_hw

p 'DEVICE_MFG'
p valDB.get_device_mfg

p 'DEVICE_BRAND'
p valDB.get_device_brand

p 'DEVICE_OSVERSION'
p valDB.get_device_osversion

p 'OSNAME'
p valDB.get_osname

p 'MINIMUM_SOFTWARE_VERSION'
p valDB.get_minimum_software_version

p 'CURRENT_SOFTWARE_VERSION'
p valDB.get_current_software_version

p 'MODULE_VERSION'
p valDB.get_module_version

p 'LAST_PROVISIONING' 
p valDB.get_last_provisioning

p 'LAST_PROVISIONING_IP'
p valDB.get_last_provisioning_ip

p 'LAST_ACCOUNTID'
p valDB.get_last_accountid

p 'LAST_UPGRADE'
p valDB.get_last_upgrade

p 'PREV_PROVISIONING'
p valDB.get_prev_provisioning

p 'PREV_PROVISIONING_IP'
p valDB.get_prev_provisioning_ip

p 'PREV_ACCOUNTID'
p valDB.get_prev_accountid

p 'PREV_UPGRADE'
p valDB.get_prev_upgrade

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'CURRENT_EXISTS'
p valDB.get_current_exists

p 'SOFTPHONE_UUID'
p valDB.get_softphone_uuid

p 'ACTIVE'
p valDB.get_active

p 'PROV_IID'
p valDB.get_prov_iid

p 'ANDROID_RELEASE'
p valDB.get_android_release

p 'ANDROID_SDKINT'
p valDB.get_android_sdkint

p 'ANDROID_FPRINT'
p valDB.get_android_fprint

valDB.closeDBConnection

