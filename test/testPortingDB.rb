require_relative '../db/PortingDB.rb'

valDB = PortingDB.new
valDB.set_porting_data(2581907)

p 'letter_id'
p valDB.get_letter_id

p 'order_returned'
p valDB.get_order_returned

p 'updated_ip'
p valDB.get_updated_ip

p 'added_ip'
p valDB.get_added_ip

p 'added'
p valDB.get_added

p 'added_csr'
p valDB.get_added_csr

p 'updated'
p valDB.get_updated

p 'updated_csr'
p valDB.get_updated_csr

p 'process_date'
p valDB.get_process_date

p 'error_message'
p valDB.get_error_message

p 'error_type'
p valDB.get_error_type

p 'status_date'
p valDB.get_status_date

p 'status_type'
p valDB.get_status_type

p 'transmit_date'
p valDB.get_transmit_date

p 'port_type'
p valDB.get_port_type

p 'reject_message'
p valDB.get_reject_message

p 'reject_code'
p valDB.get_reject_code

p 'status'
p valDB.get_status

p 'calling_party_no'
p valDB.get_calling_party_no

p 'due_date'
p valDB.get_due_date

p 'ver'
p valDB.get_ver

p 'ccna'
p valDB.get_ccna

p 'pon'
p valDB.get_pon

p 'payload'
p valDB.get_payload

p 'id'
p valDB.get_id

p 'new_spid'
p valDB.get_new_spid

p 'portbits'
p valDB.get_portbits

p 'old_spid'
p valDB.get_old_spid

p 'soa_action'
p valDB.get_soa_action

p 'sup_type'
p valDB.get_sup_type

p 'retry_count'
p valDB.get_retry_count



valDB.closeDBConnection

