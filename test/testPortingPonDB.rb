require_relative '../db/PortingPonDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = PortingPonDB.new
valDB.set_portingpon_data('12004')
	
p 'ID'
p valDB.get_id

p 'PON'
p valDB.get_pon

p 'CCNA'
p valDB.get_ccna

p 'CPNID'
p valDB.get_cpnid

p 'PON_TYPE'
p valDB.get_pon_type

p 'PORTING_ID'
p valDB.get_porting_id

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'ADDED'
p valDB.get_added

p 'PON_STATUS'
p valDB.get_pon_status

p 'CARRIER_ACCOUNT_NUMBER'
p valDB.get_carrier_account_number

p 'CARRIER_PASSCODE'
p valDB.get_carrier_passcode

p 'LOCATIONID'
p valDB.get_locationid

p 'CARRIER_SPID'
p valDB.get_carrier_spid

p 'RSUBSCRIBERID'
p valDB.get_rsubscriberid

p 'DUE_DATE'
p valDB.get_due_date

p 'CLAIMED_CARRIER'
p valDB.get_claimed_carrier

p 'ACCOUNTID'
p valDB.get_accountid

p 'ORDERID'
p valDB.get_orderid

p 'LAST_REJECTEDID'
p valDB.get_last_rejectedid

p 'SUP_COUNT'
p valDB.get_sup_count

p 'ELECTRONIC_ORDER'
p valDB.get_electronic_order

p 'ORDERDETAILID'
p valDB.get_orderdetailid

p 'BILLING_CANCEL_RESPONSE'
p valDB.get_billing_cancel_response

p 'PREVALCOUNT'
p valDB.get_prevalcount

p 'MAINNUMBER'
p valDB.get_mainnumber

valDB.closeDBConnection

